<?php

namespace XOne\Bundle\NotifierBundle\Bridge\FakeSms;

use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Notifier\Bridge\FakeSms\FakeSmsEmailTransport;
use Symfony\Component\Notifier\Bridge\FakeSms\FakeSmsLoggerTransport;
use Symfony\Component\Notifier\Bridge\FakeSms\FakeSmsTransportFactory as BaseFakeSmsTransportFactory;
use Symfony\Component\Notifier\Exception\LogicException;
use Symfony\Component\Notifier\Exception\UnsupportedSchemeException;
use Symfony\Component\Notifier\Transport\AbstractTransportFactory;
use Symfony\Component\Notifier\Transport\Dsn;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use XOne\Bundle\NotifierBundle\Entity\MessageTransaction;

/**
 * This class is copied from the vendor's {@see BaseFakeSmsTransportFactory}.
 *
 * An event dispatcher is passed to the created transports, therefore the notifier events
 * will be dispatched properly, which is necessary to create {@see MessageTransaction}.
 */
final class FakeSmsTransportFactory extends AbstractTransportFactory
{
    private ?MailerInterface $mailer;
    private ?LoggerInterface $logger;

    public function __construct(MailerInterface $mailer = null, LoggerInterface $logger = null, EventDispatcherInterface $dispatcher = null)
    {
        parent::__construct(dispatcher: $dispatcher);

        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    public function create(Dsn $dsn): FakeSmsEmailTransport|FakeSmsLoggerTransport
    {
        $scheme = $dsn->getScheme();

        if ('fakesms+email' === $scheme) {
            if (null === $this->mailer) {
                $this->throwMissingDependencyException($scheme, MailerInterface::class, 'symfony/mailer');
            }

            $mailerTransport = $dsn->getHost();
            $to = $dsn->getRequiredOption('to');
            $from = $dsn->getRequiredOption('from');

            return (new FakeSmsEmailTransport($this->mailer, $to, $from, null, $this->dispatcher))->setHost($mailerTransport);
        }

        if ('fakesms+logger' === $scheme) {
            if (null === $this->logger) {
                $this->throwMissingDependencyException($scheme, LoggerInterface::class, 'psr/log');
            }

            return new FakeSmsLoggerTransport($this->logger, null, $this->dispatcher);
        }

        throw new UnsupportedSchemeException($dsn, 'fakesms', $this->getSupportedSchemes());
    }

    protected function getSupportedSchemes(): array
    {
        return ['fakesms+email', 'fakesms+logger'];
    }

    private function throwMissingDependencyException(string $scheme, string $missingDependency, string $suggestedPackage): void
    {
        throw new LogicException(sprintf('Cannot create a transport for scheme "%s" without providing an implementation of "%s". Try running "composer require "%s"".', $scheme, $missingDependency, $suggestedPackage));
    }
}

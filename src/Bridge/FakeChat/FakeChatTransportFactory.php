<?php

namespace XOne\Bundle\NotifierBundle\Bridge\FakeChat;

use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Notifier\Bridge\FakeChat\FakeChatEmailTransport;
use Symfony\Component\Notifier\Bridge\FakeChat\FakeChatLoggerTransport;
use Symfony\Component\Notifier\Bridge\FakeChat\FakeChatTransportFactory as BaseFakeChatTransportFactory;
use Symfony\Component\Notifier\Exception\LogicException;
use Symfony\Component\Notifier\Exception\UnsupportedSchemeException;
use Symfony\Component\Notifier\Transport\AbstractTransportFactory;
use Symfony\Component\Notifier\Transport\Dsn;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use XOne\Bundle\NotifierBundle\Entity\MessageTransaction;

/**
 * This class is copied from the vendor's {@see BaseFakeChatTransportFactory}.
 *
 * An event dispatcher is passed to the created transports, therefore the notifier events
 * will be dispatched properly, which is necessary to create {@see MessageTransaction}.
 */
final class FakeChatTransportFactory extends AbstractTransportFactory
{
    private ?MailerInterface $mailer;
    private ?LoggerInterface $logger;

    public function __construct(MailerInterface $mailer = null, LoggerInterface $logger = null, EventDispatcherInterface $dispatcher = null)
    {
        parent::__construct(dispatcher: $dispatcher);

        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    public function create(Dsn $dsn): FakeChatEmailTransport|FakeChatLoggerTransport
    {
        $scheme = $dsn->getScheme();

        if ('fakechat+email' === $scheme) {
            if (null === $this->mailer) {
                $this->throwMissingDependencyException($scheme, MailerInterface::class, 'symfony/mailer');
            }

            $mailerTransport = $dsn->getHost();
            $to = $dsn->getRequiredOption('to');
            $from = $dsn->getRequiredOption('from');

            return (new FakeChatEmailTransport($this->mailer, $to, $from, null, $this->dispatcher))->setHost($mailerTransport);
        }

        if ('fakechat+logger' === $scheme) {
            if (null === $this->logger) {
                $this->throwMissingDependencyException($scheme, LoggerInterface::class, 'psr/log');
            }

            return new FakeChatLoggerTransport($this->logger, null, $this->dispatcher);
        }

        throw new UnsupportedSchemeException($dsn, 'fakechat', $this->getSupportedSchemes());
    }

    protected function getSupportedSchemes(): array
    {
        return ['fakechat+email', 'fakechat+logger'];
    }

    private function throwMissingDependencyException(string $scheme, string $missingDependency, string $suggestedPackage): void
    {
        throw new LogicException(sprintf('Cannot create a transport for scheme "%s" without providing an implementation of "%s". Try running "composer require "%s"".', $scheme, $missingDependency, $suggestedPackage));
    }
}

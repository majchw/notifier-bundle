<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Recipient;

use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;
use Symfony\Component\Notifier\Recipient\NoRecipient;
use Symfony\Component\Notifier\Recipient\SmsRecipientInterface;
use XOne\Bundle\NotifierBundle\Exception\LogicException;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Notification\PersistentMessageNotification;

/**
 * This class represents a fake recipient similar to a built-in {@see NoRecipient},
 * but additionally implements the email and sms interfaces, making it supported by those channels.
 */
class NullRecipient implements EmailRecipientInterface, SmsRecipientInterface
{
    public function getEmail(): string
    {
        throw $this->createException();
    }

    public function getPhone(): string
    {
        throw $this->createException();
    }

    private function createException(): LogicException
    {
        $message = 'The NullRecipient shouldn\'t be used to retrieve the email nor the phone number!';
        $message .= sprintf(' Instead, retrieve an instance of the %s class, which contains valid recipients.', MessageInterface::class);
        $message .= sprintf(' If you have access to an instance of %s class, you can use its "getPersistentMessage()" method to retrieve a message.', PersistentMessageNotification::class);

        return new LogicException($message);
    }
}

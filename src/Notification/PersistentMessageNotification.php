<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Notification;

use Symfony\Component\Notifier\Message\ChatMessage as BaseChatMessage;
use Symfony\Component\Notifier\Message\EmailMessage as BaseEmailMessage;
use Symfony\Component\Notifier\Message\PushMessage as BasePushMessage;
use Symfony\Component\Notifier\Message\SmsMessage as BaseSmsMessage;
use Symfony\Component\Notifier\Notification\ChatNotificationInterface;
use Symfony\Component\Notifier\Notification\EmailNotificationInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Notification\PushNotificationInterface;
use Symfony\Component\Notifier\Notification\SmsNotificationInterface;
use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;
use Symfony\Component\Notifier\Recipient\RecipientInterface;
use Symfony\Component\Notifier\Recipient\SmsRecipientInterface;
use XOne\Bundle\NotifierBundle\Message\ChatMessage;
use XOne\Bundle\NotifierBundle\Message\EmailMessage;
use XOne\Bundle\NotifierBundle\Message\PushMessage;
use XOne\Bundle\NotifierBundle\Message\SmsMessage;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;

/**
 * Extension of the {@see Notification} based on the persistent {@see MessageInterface}.
 */
class PersistentMessageNotification extends Notification implements EmailNotificationInterface, SmsNotificationInterface, ChatNotificationInterface, PushNotificationInterface
{
    public function __construct(
        private readonly MessageInterface $persistentMessage,
    ) {
        parent::__construct();

        $this
            ->subject($persistentMessage->getSubject() ?? '')
            ->content($persistentMessage->getContent() ?? '')
            ->channels([$persistentMessage->getChannel()])
        ;
    }

    public function getPersistentMessage(): MessageInterface
    {
        return $this->persistentMessage;
    }

    public function asEmailMessage(EmailRecipientInterface $recipient, string $transport = null): ?BaseEmailMessage
    {
        return EmailMessage::fromNotification($this, $recipient, $transport);
    }

    public function asSmsMessage(SmsRecipientInterface $recipient, string $transport = null): ?BaseSmsMessage
    {
        return SmsMessage::fromNotification($this, $recipient, $transport);
    }

    public function asChatMessage(RecipientInterface $recipient, string $transport = null): ?BaseChatMessage
    {
        return ChatMessage::fromNotification($this, $transport);
    }

    public function asPushMessage(RecipientInterface $recipient, string $transport = null): ?BasePushMessage
    {
        return PushMessage::fromNotification($this, $transport);
    }
}

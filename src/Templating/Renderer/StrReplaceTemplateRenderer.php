<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Templating\Renderer;

class StrReplaceTemplateRenderer implements TemplateRendererInterface
{
    public function render(string $template, array $variables): string
    {
        foreach ($variables as $name => $value) {
            $template = str_replace(
                search: ["{{{$name}}}", "{{ $name }}", "{{ $name}}", "{{{$name} }}"],
                replace: (string) $value,
                subject: $template,
            );
        }

        return $template;
    }
}

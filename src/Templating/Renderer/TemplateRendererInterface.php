<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Templating\Renderer;

interface TemplateRendererInterface
{
    /**
     * Renders given template string, replacing its content variables with given values.
     *
     * @param array<string, mixed> $variables key-value pairs of variables to replace in the template
     */
    public function render(string $template, array $variables): string;
}

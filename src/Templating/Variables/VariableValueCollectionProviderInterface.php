<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Templating\Variables;

interface VariableValueCollectionProviderInterface
{
    /**
     * Returns values of given variables, from given subject.
     * Additionally, a default value can be provided to use when providers cannot return a variable value.
     *
     * @param array<string> $variables variable names to retrieve values for
     * @param mixed         $subject   data to retrieve the values of the variables from, e.g. Product entity
     * @param mixed         $default   value returned for a variable when providers are unable to return its value
     *
     * @return array<string, mixed> with variable name as key
     */
    public function getValues(array $variables, mixed $subject = null, mixed $default = null): array;
}

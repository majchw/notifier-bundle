<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Templating\Variables;

use XOne\Bundle\NotifierBundle\Exception\VariableValueProviderException;

interface VariableValueProviderInterface
{
    /**
     * Returns the value of variable of given name, from given subject.
     *
     * @param string $name    name of the variable, e.g. "product_name"
     * @param mixed  $subject data to retrieve the value of the variable from, e.g. Product entity
     *
     * @throws VariableValueProviderException if the provider is unable to return a value
     */
    public function getValue(string $name, mixed $subject = null): mixed;
}

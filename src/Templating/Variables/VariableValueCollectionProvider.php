<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Templating\Variables;

use XOne\Bundle\NotifierBundle\Exception\VariableValueProviderException;

class VariableValueCollectionProvider implements VariableValueCollectionProviderInterface
{
    public function __construct(
        private readonly VariableValueProviderInterface $variableValueProvider,
    ) {
    }

    public function getValues(array $variables, mixed $subject = null, mixed $default = null): array
    {
        $values = [];

        foreach ($variables as $variable) {
            $variable = (string) $variable;

            try {
                $values[$variable] = $this->variableValueProvider->getValue($variable, $subject);
            } catch (VariableValueProviderException) {
                $values[$variable] = $default;
            }
        }

        return $values;
    }
}

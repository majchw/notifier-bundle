<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Templating\Variables;

use XOne\Bundle\NotifierBundle\Exception\VariableValueProviderException;

class ChainVariableValueProvider implements VariableValueProviderInterface
{
    /**
     * @param iterable<VariableValueProviderInterface> $providers
     */
    public function __construct(
        private readonly iterable $providers = [],
    ) {
    }

    public function getValue(string $name, mixed $subject = null): mixed
    {
        foreach ($this->providers as $provider) {
            try {
                return $provider->getValue($name, $subject);
            } catch (VariableValueProviderException) {
                continue;
            }
        }

        throw new VariableValueProviderException();
    }
}

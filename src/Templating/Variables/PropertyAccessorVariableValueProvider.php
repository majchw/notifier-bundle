<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Templating\Variables;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use XOne\Bundle\NotifierBundle\Exception\VariableValueProviderException;

class PropertyAccessorVariableValueProvider implements VariableValueProviderInterface
{
    public function __construct(
        private ?PropertyAccessorInterface $propertyAccessor = null,
    ) {
        $this->propertyAccessor ??= $this->createPropertyAccessor();
    }

    public function getValue(string $name, mixed $subject = null): mixed
    {
        if (is_array($subject) && $this->propertyAccessor->isReadable($subject, "[$name]")) {
            return $this->propertyAccessor->getValue($subject, "[$name]");
        }

        if (is_object($subject) && $this->propertyAccessor->isReadable($subject, $name)) {
            return $this->propertyAccessor->getValue($subject, $name);
        }

        throw new VariableValueProviderException();
    }

    private function createPropertyAccessor(): PropertyAccessorInterface
    {
        return PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->enableExceptionOnInvalidPropertyPath()
            ->enableMagicCall()
            ->enableMagicGet()
            ->getPropertyAccessor()
        ;
    }
}

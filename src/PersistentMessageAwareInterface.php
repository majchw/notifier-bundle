<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle;

use XOne\Bundle\NotifierBundle\Model\MessageInterface;

interface PersistentMessageAwareInterface
{
    /**
     * Retrieves identifier of the persistent {@see MessageInterface}.
     */
    public function getPersistentMessageId(): ?int;
}

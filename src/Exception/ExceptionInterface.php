<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Exception;

interface ExceptionInterface extends \Throwable
{
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Exception;

class VariableValueProviderException extends \Exception implements ExceptionInterface
{
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Exception;

class BadMethodCallException extends \BadMethodCallException implements ExceptionInterface
{
}

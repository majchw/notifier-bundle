<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Exception;

class LogicException extends \LogicException implements ExceptionInterface
{
}

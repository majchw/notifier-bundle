<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Repository;

use Doctrine\Persistence\ObjectRepository;
use XOne\Bundle\NotifierBundle\Model\MessageTemplateInterface;

/**
 * @extends ObjectRepository<MessageTemplateInterface>
 *
 * @method MessageTemplateInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method MessageTemplateInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method MessageTemplateInterface[]    findAll()
 * @method MessageTemplateInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
interface MessageTemplateRepositoryInterface extends ObjectRepository
{
    public function save(MessageTemplateInterface $messageTemplate): void;
}

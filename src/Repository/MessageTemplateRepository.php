<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use XOne\Bundle\NotifierBundle\Entity\MessageTemplate;
use XOne\Bundle\NotifierBundle\Model\MessageTemplateInterface;

class MessageTemplateRepository extends ServiceEntityRepository implements MessageTemplateRepositoryInterface
{
    public function __construct(ManagerRegistry $registry, string $entityClass = MessageTemplate::class)
    {
        parent::__construct($registry, $entityClass);
    }

    public function save(MessageTemplateInterface $messageTemplate): void
    {
        $this->getEntityManager()->persist($messageTemplate);
        $this->getEntityManager()->flush();
    }
}

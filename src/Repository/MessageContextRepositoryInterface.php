<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Repository;

use Doctrine\Persistence\ObjectRepository;
use XOne\Bundle\NotifierBundle\Model\MessageContextInterface;

/**
 * @extends ObjectRepository<MessageContextInterface>
 *
 * @method MessageContextInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method MessageContextInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method MessageContextInterface[]    findAll()
 * @method MessageContextInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
interface MessageContextRepositoryInterface extends ObjectRepository
{
    public function save(MessageContextInterface $messageContext): void;
}

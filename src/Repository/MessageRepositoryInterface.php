<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Repository;

use Doctrine\Persistence\ObjectRepository;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;

/**
 * @extends ObjectRepository<MessageInterface>
 *
 * @method MessageInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method MessageInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method MessageInterface[]    findAll()
 * @method MessageInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
interface MessageRepositoryInterface extends ObjectRepository
{
    public function save(MessageInterface $message): void;
}

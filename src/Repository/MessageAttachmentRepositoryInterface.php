<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Repository;

use Doctrine\Persistence\ObjectRepository;
use XOne\Bundle\NotifierBundle\Model\MessageAttachmentInterface;

/**
 * @extends ObjectRepository<MessageAttachmentInterface>
 *
 * @method MessageAttachmentInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method MessageAttachmentInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method MessageAttachmentInterface[]    findAll()
 * @method MessageAttachmentInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
interface MessageAttachmentRepositoryInterface extends ObjectRepository
{
    public function save(MessageAttachmentInterface $messageContext): void;
}

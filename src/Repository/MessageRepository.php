<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use XOne\Bundle\NotifierBundle\Entity\Message;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;

class MessageRepository extends ServiceEntityRepository implements MessageRepositoryInterface
{
    public function __construct(ManagerRegistry $registry, string $entityClass = Message::class)
    {
        parent::__construct($registry, $entityClass);
    }

    public function save(MessageInterface $message): void
    {
        $this->getEntityManager()->persist($message);
        $this->getEntityManager()->flush();
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Repository;

use Doctrine\Persistence\ObjectRepository;
use XOne\Bundle\NotifierBundle\Model\MessageTransactionInterface;

/**
 * @extends ObjectRepository<MessageTransactionInterface>
 *
 * @method MessageTransactionInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method MessageTransactionInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method MessageTransactionInterface[]    findAll()
 * @method MessageTransactionInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
interface MessageTransactionRepositoryInterface extends ObjectRepository
{
    public function save(MessageTransactionInterface $messageTransaction): void;

    public function findOneByTransportMessageId(string $transportMessageId): ?MessageTransactionInterface;
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use XOne\Bundle\NotifierBundle\Entity\MessageContext;
use XOne\Bundle\NotifierBundle\Model\MessageContextInterface;

class MessageContextRepository extends ServiceEntityRepository implements MessageContextRepositoryInterface
{
    public function __construct(ManagerRegistry $registry, string $entityClass = MessageContext::class)
    {
        parent::__construct($registry, $entityClass);
    }

    public function save(MessageContextInterface $messageContext): void
    {
        $this->getEntityManager()->persist($messageContext);
        $this->getEntityManager()->flush();
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use XOne\Bundle\NotifierBundle\Entity\MessageAttachment;
use XOne\Bundle\NotifierBundle\Model\MessageAttachmentInterface;

class MessageAttachmentRepository extends ServiceEntityRepository implements MessageAttachmentRepositoryInterface
{
    public function __construct(ManagerRegistry $registry, string $entityClass = MessageAttachment::class)
    {
        parent::__construct($registry, $entityClass);
    }

    public function save(MessageAttachmentInterface $messageAttachment): void
    {
        $this->getEntityManager()->persist($messageAttachment);
        $this->getEntityManager()->flush();
    }
}

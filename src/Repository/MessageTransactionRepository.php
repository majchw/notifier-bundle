<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use XOne\Bundle\NotifierBundle\Entity\MessageTransaction;
use XOne\Bundle\NotifierBundle\Model\MessageTransactionInterface;

class MessageTransactionRepository extends ServiceEntityRepository implements MessageTransactionRepositoryInterface
{
    public function __construct(ManagerRegistry $registry, string $entityClass = MessageTransaction::class)
    {
        parent::__construct($registry, $entityClass);
    }

    public function save(MessageTransactionInterface $messageTransaction): void
    {
        $this->getEntityManager()->persist($messageTransaction);
        $this->getEntityManager()->flush();
    }

    public function findOneByTransportMessageId(string $transportMessageId): ?MessageTransactionInterface
    {
        return $this->findOneBy(['transportMessageId' => $transportMessageId]);
    }
}

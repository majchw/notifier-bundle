<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Mailer;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\PersistentMessageAwareInterface;
use XOne\Bundle\NotifierBundle\PersistentMessageAwareTrait;

/**
 * Wrapper around the base {@see TemplatedEmail} to hold reference to the persistent {@see MessageInterface}.
 * This is used only by the mailer, and is **required** by the bundle event subscribers!
 */
class PersistentMessageEmail extends TemplatedEmail implements PersistentMessageAwareInterface
{
    use PersistentMessageAwareTrait;

    public function updateFromPersistentMessage(MessageInterface $message): void
    {
        $this->persistentMessageId = $message->getId();
    }
}

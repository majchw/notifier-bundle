<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Mailer;

use Symfony\Component\Mailer\Envelope;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\PersistentMessageAwareInterface;
use XOne\Bundle\NotifierBundle\PersistentMessageAwareTrait;

/**
 * Wrapper around the base {@see Envelope} to hold reference to the persistent {@see MessageInterface}.
 * This envelope is used only by the mailer, and is **required** by the bundle event subscribers!
 *
 * Note: original envelope can be retrieved using the {@see PersistentMessageEnvelope::getOriginalEnvelope()} method.
 */
class PersistentMessageEnvelope extends Envelope implements PersistentMessageAwareInterface
{
    use PersistentMessageAwareTrait;

    public function __construct(
        int $persistentMessageId = null,
        private readonly ?Envelope $originalEnvelope = null
    ) {
        $this->persistentMessageId = $persistentMessageId;

        if ($this->originalEnvelope) {
            parent::__construct($this->originalEnvelope->getSender(), $this->originalEnvelope->getRecipients());
        }
    }

    public function getOriginalEnvelope(): ?Envelope
    {
        return $this->originalEnvelope;
    }
}

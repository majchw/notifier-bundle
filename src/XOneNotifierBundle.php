<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle;

use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use XOne\Bundle\NotifierBundle\Doctrine\Types\AddressJsonType;
use XOne\Bundle\NotifierBundle\Model\MessageAttachmentInterface;
use XOne\Bundle\NotifierBundle\Model\MessageContextInterface;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Model\MessageTemplateInterface;
use XOne\Bundle\NotifierBundle\Model\MessageTransactionInterface;
use XOne\Bundle\NotifierBundle\Templating\Variables\VariableValueProviderInterface;

class XOneNotifierBundle extends AbstractBundle
{
    public function prependExtension(ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->extension('doctrine', [
            'dbal' => [
                'types' => [
                    AddressJsonType::NAME => AddressJsonType::class,
                ],
            ],
        ]);

        $config = $builder->getExtensionConfig('x_one_notifier')[0];

        $container->extension('doctrine', [
            'orm' => [
                'resolve_target_entities' => array_filter([
                    MessageInterface::class => $config['entities']['message'] ?? null,
                    MessageTransactionInterface::class => $config['entities']['message_transaction'] ?? null,
                    MessageAttachmentInterface::class => $config['entities']['message_attachment'] ?? null,
                    MessageContextInterface::class => $config['entities']['message_context'] ?? null,
                    MessageTemplateInterface::class => $config['entities']['message_template'] ?? null,
                ]),
            ],
        ]);
    }

    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import('../config/services.php');

        $container->parameters()
            ->set('x_one_notifier.entity.message.class', $config['entities']['message'])
            ->set('x_one_notifier.entity.message_attachment.class', $config['entities']['message_attachment'])
            ->set('x_one_notifier.entity.message_transaction.class', $config['entities']['message_transaction'])
            ->set('x_one_notifier.entity.message_context.class', $config['entities']['message_context'])
            ->set('x_one_notifier.entity.message_template.class', $config['entities']['message_template'])
        ;

        $container->services()
            ->get('x_one_notifier.event_subscriber.sent_message')
            ->arg('$debug', $config['save_mailer_debug'])
        ;

        $builder
            ->registerForAutoconfiguration(VariableValueProviderInterface::class)
            ->addTag('x_one_notifier.templating.variable_value_provider')
        ;
    }

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->import('../config/definition.php');
    }
}

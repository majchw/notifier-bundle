<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Model;

interface MessageContextInterface
{
    public function getId(): ?int;

    public function getSymbol(): ?string;

    public function setSymbol(?string $symbol): static;

    public function getName(): ?string;

    public function setName(?string $name): static;

    public function getVariables(): array;

    public function setVariables(array $variables): static;

    public function getChannels(): array;

    public function setChannels(array $channels): static;
}

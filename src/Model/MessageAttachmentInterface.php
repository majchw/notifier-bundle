<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Model;

use Symfony\Component\HttpFoundation\File\File;

interface MessageAttachmentInterface
{
    public function getId(): ?int;

    public function getMessage(): ?MessageInterface;

    public function setMessage(?MessageInterface $message): static;

    public function getName(): ?string;

    public function setName(?string $name): static;

    public function getFilename(): ?string;

    public function setFilename(?string $filename): static;

    public function getFile(): ?File;

    public function getContentType(): ?string;

    public function setContentType(?string $contentType): static;
}

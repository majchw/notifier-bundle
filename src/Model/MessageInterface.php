<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Model;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\Notifier\Message\MessageOptionsInterface;
use XOne\Bundle\NotifierBundle\Model\ValueObject\AddressInterface;

interface MessageInterface
{
    public function getId(): ?int;

    public function getSubject(): ?string;

    public function setSubject(?string $subject): static;

    public function getContent(): ?string;

    public function setContent(?string $content): static;

    public function getChannel(): ?string;

    public function setChannel(?string $channel): static;

    public function getTransport(): ?string;

    public function setTransport(?string $transport): static;

    /**
     * @return array<AddressInterface>
     */
    public function getFrom(): array;

    /**
     * @param array<AddressInterface> $from
     */
    public function setFrom(array $from): static;

    public function addFrom(AddressInterface $address): static;

    /**
     * @return array<AddressInterface>
     */
    public function getTo(): array;

    /**
     * @param array<AddressInterface> $to
     */
    public function setTo(array $to): static;

    public function addTo(AddressInterface $address): static;

    /**
     * @return array<AddressInterface>
     */
    public function getCc(): array;

    /**
     * @param array<AddressInterface> $cc
     */
    public function setCc(array $cc): static;

    public function addCc(AddressInterface $address): static;

    /**
     * @return array<AddressInterface>
     */
    public function getBcc(): array;

    /**
     * @param array<AddressInterface> $bcc
     */
    public function setBcc(array $bcc): static;

    public function addBcc(AddressInterface $address): static;

    /**
     * @return array<AddressInterface>
     */
    public function getReplyTo(): array;

    /**
     * @param array<AddressInterface> $replyTo
     */
    public function setReplyTo(array $replyTo): static;

    public function addReplyTo(AddressInterface $address): static;

    public function getSender(): ?AddressInterface;

    public function setSender(?AddressInterface $sender): void;

    public function getReturnPath(): ?AddressInterface;

    public function setReturnPath(?AddressInterface $returnPath): void;

    public function getPriority(): ?int;

    public function setPriority(?int $priority): void;

    public function getOptions(): MessageOptionsInterface;

    public function setOptions(MessageOptionsInterface $options): static;

    public function getRecipientId(): ?string;

    public function setRecipientId(?string $recipientId): void;

    /**
     * @return Collection<MessageAttachmentInterface>
     */
    public function getMessageAttachments(): Collection;

    public function addMessageAttachment(MessageAttachmentInterface $messageAttachment): static;

    public function removeMessageAttachment(MessageAttachmentInterface $messageAttachment): static;
}

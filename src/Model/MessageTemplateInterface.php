<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Model;

interface MessageTemplateInterface
{
    public function getName(): ?string;

    public function setName(?string $name): static;

    public function getSubject(): ?string;

    public function setSubject(?string $subject): static;

    public function getContent(): ?string;

    public function setContent(?string $content): static;

    public function getVariables(): array;

    public function setVariables(array $variables): static;

    public function getChannels(): array;

    public function setChannels(array $channels): static;
}

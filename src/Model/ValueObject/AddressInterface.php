<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Model\ValueObject;

use Symfony\Component\Mime\Address as MimeAddress;
use Symfony\Component\Notifier\Recipient\RecipientInterface as NotifierRecipientInterface;

/**
 * Generic address value object, compatible with **all** built-in channels.
 *
 * Note: in case of SMS messages, the {@see AddressInterface::getAddress()} should be a phone number.
 */
interface AddressInterface extends \JsonSerializable
{
    public static function fromMimeAddress(MimeAddress $mimeAddress): self;

    public function toMimeAddress(): MimeAddress;

    public function toNotifierRecipient(): NotifierRecipientInterface;

    public function getAddress(): ?string;

    public function setAddress(?string $address): void;

    public function getName(): ?string;

    public function setName(?string $name): void;
}

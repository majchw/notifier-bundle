<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Model;

interface MessageTransactionInterface
{
    public function getId(): ?int;

    public function getMessage(): ?MessageInterface;

    public function setMessage(?MessageInterface $message): static;

    public function isSuccessful(): bool;

    public function setSuccessful(bool $successful): static;

    public function getTransportMessageId(): ?string;

    public function setTransportMessageId(?string $transportMessageId): static;

    public function getException(): ?string;

    public function setException(?string $exception): static;

    public function getDebug(): ?string;

    public function setDebug(?string $debug): static;
}

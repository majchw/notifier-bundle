<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle;

trait PersistentMessageAwareTrait
{
    private ?int $persistentMessageId = null;

    public function getPersistentMessageId(): ?int
    {
        return $this->persistentMessageId;
    }
}

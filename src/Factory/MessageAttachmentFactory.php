<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Factory;

use XOne\Bundle\NotifierBundle\Entity\MessageAttachment;
use XOne\Bundle\NotifierBundle\Model\MessageAttachmentInterface;

class MessageAttachmentFactory implements MessageAttachmentFactoryInterface
{
    public function __construct(
        private readonly string $class = MessageAttachment::class,
    ) {
    }

    public function create(string $filename, string $name = null, string $contentType = null): MessageAttachmentInterface
    {
        return (new $this->class())
            ->setFilename($filename)
            ->setName($name)
            ->setContentType($contentType)
        ;
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Factory;

use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Model\MessageTransactionInterface;

interface MessageTransactionFactoryInterface
{
    public function create(MessageInterface $message = null): MessageTransactionInterface;

    public function createSuccessful(MessageInterface $message = null, string $transportMessageId = null): MessageTransactionInterface;

    public function createFailed(MessageInterface $message = null, \Throwable $exception = null): MessageTransactionInterface;
}

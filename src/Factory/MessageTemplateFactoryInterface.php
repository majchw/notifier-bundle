<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Factory;

use XOne\Bundle\NotifierBundle\Entity\MessageContext;
use XOne\Bundle\NotifierBundle\Model\MessageTemplateInterface;

interface MessageTemplateFactoryInterface
{
    public function create(string $name, string $subject = null, string $content = null): MessageTemplateInterface;

    public function createFromMessageContext(MessageContext $messageContext, string $content = null): MessageTemplateInterface;
}

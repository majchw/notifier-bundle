<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Factory;

use XOne\Bundle\NotifierBundle\Entity\MessageTransaction;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Model\MessageTransactionInterface;

class MessageTransactionFactory implements MessageTransactionFactoryInterface
{
    /**
     * @param class-string<MessageTransactionInterface> $class
     */
    public function __construct(
        private readonly string $class = MessageTransaction::class,
    ) {
    }

    public function create(MessageInterface $message = null): MessageTransactionInterface
    {
        return (new $this->class())->setMessage($message);
    }

    public function createSuccessful(MessageInterface $message = null, string $transportMessageId = null): MessageTransactionInterface
    {
        return $this->create($message)
            ->setSuccessful(true)
            ->setTransportMessageId($transportMessageId)
        ;
    }

    public function createFailed(MessageInterface $message = null, \Throwable $exception = null): MessageTransactionInterface
    {
        return $this->create($message)
            ->setSuccessful(false)
            ->setException($exception?->getMessage())
        ;
    }
}

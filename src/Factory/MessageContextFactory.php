<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Factory;

use XOne\Bundle\NotifierBundle\Entity\MessageContext;
use XOne\Bundle\NotifierBundle\Model\MessageContextInterface;

class MessageContextFactory implements MessageContextFactoryInterface
{
    /**
     * @param class-string<MessageContextInterface> $class
     */
    public function __construct(
        private readonly string $class = MessageContext::class,
    ) {
    }

    public function create(string $symbol = null, string $name = null, array $variables = [], array $channels = []): MessageContextInterface
    {
        return (new $this->class())
            ->setSymbol($symbol)
            ->setName($name)
            ->setVariables($variables)
            ->setChannels($channels)
        ;
    }
}

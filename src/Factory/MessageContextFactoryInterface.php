<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Factory;

use XOne\Bundle\NotifierBundle\Model\MessageContextInterface;

interface MessageContextFactoryInterface
{
    public function create(string $symbol = null, string $name = null, array $variables = [], array $channels = []): MessageContextInterface;
}

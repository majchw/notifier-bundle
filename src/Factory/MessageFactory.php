<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Factory;

use XOne\Bundle\NotifierBundle\Entity\Message;
use XOne\Bundle\NotifierBundle\Exception\LogicException;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Model\MessageTemplateInterface;
use XOne\Bundle\NotifierBundle\Templating\Renderer\TemplateRendererInterface;
use XOne\Bundle\NotifierBundle\Templating\Variables\VariableValueCollectionProviderInterface;

class MessageFactory implements MessageFactoryInterface
{
    public const DEFAULT_CHANNEL = 'email';

    /**
     * @param class-string<MessageInterface> $class
     */
    public function __construct(
        private readonly TemplateRendererInterface $templateRenderer,
        private readonly VariableValueCollectionProviderInterface $variableValueCollectionProvider,
        private readonly string $class = Message::class,
        private readonly string $defaultChannel = self::DEFAULT_CHANNEL,
    ) {
    }

    /**
     * Creates a new instance of {@see MessageInterface} with given subject, content and channel.
     *
     * @param string|null $channel if not given, {@see self::DEFAULT_CHANNEL} is used
     */
    public function create(string $subject = null, string $content = null, string $channel = null): MessageInterface
    {
        return (new $this->class())
            ->setSubject($subject)
            ->setContent($content)
            ->setChannel($channel ?? $this->defaultChannel)
        ;
    }

    /**
     * Creates a new instance of {@see MessageInterface} based on the {@see MessageTemplateInterface}.
     * A variable subject can be provided to use in the template rendering process.
     *
     * @param string|null $channel the channel must be supported by the given {@see MessageTemplateInterface},
     *                             otherwise the exception will be thrown. If not given, first channel supported by
     *                             the given {@see MessageTemplateInterface} will be used.
     */
    public function createTemplated(MessageTemplateInterface $messageTemplate, mixed $variableSubject = null, string $channel = null): MessageInterface
    {
        return $this->create(
            subject: $this->getSubject($messageTemplate, $variableSubject),
            content: $this->getContent($messageTemplate, $variableSubject),
            channel: $this->getChannel($messageTemplate, $channel),
        );
    }

    /**
     * Creates a new instance of {@see MessageInterface} with "email" channel set by default.
     */
    public function createEmail(): MessageInterface
    {
        return $this->create()->setChannel('email');
    }

    /**
     * Creates a new instance of {@see MessageInterface} based on the {@see MessageTemplateInterface},
     * with "email" channel set as default. A variable subject can be provided to use in the template rendering process.
     */
    public function createTemplatedEmail(MessageTemplateInterface $messageTemplate, mixed $variableSubject = null): MessageInterface
    {
        return $this->createTemplated($messageTemplate, $variableSubject, 'email');
    }

    /**
     * Creates a new instance of {@see MessageInterface} with "sms" channel set by default.
     */
    public function createSms(): MessageInterface
    {
        return $this->create()->setChannel('sms');
    }

    /**
     * Creates a new instance of {@see MessageInterface} based on the {@see MessageTemplateInterface},
     * with "sms" channel set as default. A variable subject can be provided to use in the template rendering process.
     */
    public function createTemplatedSms(MessageTemplateInterface $messageTemplate, mixed $variableSubject = null): MessageInterface
    {
        return $this->createTemplated($messageTemplate, $variableSubject, 'sms');
    }

    /**
     * Creates a new instance of {@see MessageInterface} with "chat" channel set by default.
     */
    public function createChat(): MessageInterface
    {
        return $this->create()->setChannel('chat');
    }

    /**
     * Creates a new instance of {@see MessageInterface} based on the {@see MessageTemplateInterface},
     * with "chat" channel set as default. A variable subject can be provided to use in the template rendering process.
     */
    public function createTemplatedChat(MessageTemplateInterface $messageTemplate, mixed $variableSubject = null): MessageInterface
    {
        return $this->createTemplated($messageTemplate, $variableSubject, 'chat');
    }

    /**
     * Creates a new instance of {@see MessageInterface} with "push" channel set by default.
     */
    public function createPush(): MessageInterface
    {
        return $this->create()->setChannel('push');
    }

    /**
     * Creates a new instance of {@see MessageInterface} based on the {@see MessageTemplateInterface},
     * with "push" channel set as default. A variable subject can be provided to use in the template rendering process.
     */
    public function createTemplatedPush(MessageTemplateInterface $messageTemplate, mixed $variableSubject = null): MessageInterface
    {
        return $this->createTemplated($messageTemplate, $variableSubject, 'push');
    }

    /**
     * Retrieves rendered subject (with variables replaces with values) of the given {@see MessageTemplateInterface}.
     * A variable subject can be provided to use in the template rendering process.
     */
    private function getSubject(MessageTemplateInterface $messageTemplate, mixed $subject = null): string
    {
        return $this->templateRenderer->render(
            template: $messageTemplate->getSubject() ?? '',
            variables: $this->getMessageTemplateVariableValues($messageTemplate, $subject),
        );
    }

    /**
     * Retrieves rendered content (with variables replaces with values) of the given {@see MessageTemplateInterface}.
     * A variable subject can be provided to use in the template rendering process.
     */
    private function getContent(MessageTemplateInterface $messageTemplate, mixed $subject = null): string
    {
        return $this->templateRenderer->render(
            template: $messageTemplate->getContent() ?? '',
            variables: $this->getMessageTemplateVariableValues($messageTemplate, $subject),
        );
    }

    /**
     * Retrieves values of the variables required by the given {@see MessageTemplateInterface}.
     * A variable subject can be provided to pass to the {@see VariableValueCollectionProviderInterface}.
     *
     * @return array<string, mixed> with variable name as key
     */
    private function getMessageTemplateVariableValues(MessageTemplateInterface $messageTemplate, mixed $subject = null): array
    {
        return $this->variableValueCollectionProvider->getValues($messageTemplate->getVariables(), $subject);
    }

    /**
     * Retrieves channel of the {@see MessageTemplateInterface}, using its first supported channel as default.
     * A channel can be provided as argument, to check whether the given message template supports it.
     */
    private function getChannel(MessageTemplateInterface $messageTemplate, string $channel = null): ?string
    {
        $channels = $messageTemplate->getChannels();

        if (null !== $channel && !in_array($channel, $channels)) {
            $error = sprintf('Channel "%s" is not supported by the message template.', $channel);
            $error .= sprintf(' The message template supports following channels: "%s"', implode('", "', $channels));

            throw new LogicException($error);
        }

        if (null === $channel && count($channels) > 1) {
            $error = 'Template used to create a message has more than one channel.';
            $error .= sprintf(' You must specify which channel ("%s") the message should use.', implode('", "', $channels));

            throw new LogicException($error);
        }

        return $channel ?? (current($channels) ?: null);
    }
}

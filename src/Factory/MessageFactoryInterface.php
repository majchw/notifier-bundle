<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Factory;

use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Model\MessageTemplateInterface;

interface MessageFactoryInterface
{
    public function create(): MessageInterface;

    public function createTemplated(MessageTemplateInterface $messageTemplate, mixed $variableSubject = null, string $channel = null): MessageInterface;

    public function createEmail(): MessageInterface;

    public function createTemplatedEmail(MessageTemplateInterface $messageTemplate, mixed $variableSubject = null): MessageInterface;

    public function createSms(): MessageInterface;

    public function createTemplatedSms(MessageTemplateInterface $messageTemplate, mixed $variableSubject = null): MessageInterface;

    public function createChat(): MessageInterface;

    public function createTemplatedChat(MessageTemplateInterface $messageTemplate, mixed $variableSubject = null): MessageInterface;

    public function createPush(): MessageInterface;

    public function createTemplatedPush(MessageTemplateInterface $messageTemplate, mixed $variableSubject = null): MessageInterface;
}

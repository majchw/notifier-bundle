<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Factory;

use XOne\Bundle\NotifierBundle\Model\MessageAttachmentInterface;

interface MessageAttachmentFactoryInterface
{
    public function create(string $filename, string $name = null, string $contentType = null): MessageAttachmentInterface;
}

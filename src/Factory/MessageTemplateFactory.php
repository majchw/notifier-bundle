<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Factory;

use XOne\Bundle\NotifierBundle\Entity\MessageContext;
use XOne\Bundle\NotifierBundle\Entity\MessageTemplate;
use XOne\Bundle\NotifierBundle\Model\MessageTemplateInterface;

class MessageTemplateFactory implements MessageTemplateFactoryInterface
{
    /**
     * @param class-string<MessageTemplateInterface> $class
     */
    public function __construct(
        private readonly string $class = MessageTemplate::class,
    ) {
    }

    public function create(string $name, string $subject = null, string $content = null, array $variables = [], array $channels = []): MessageTemplateInterface
    {
        return (new $this->class())
            ->setName($name)
            ->setSubject($subject)
            ->setContent($content)
            ->setVariables($variables)
            ->setChannels($channels)
        ;
    }

    public function createFromMessageContext(MessageContext $messageContext, string $content = null): MessageTemplateInterface
    {
        return $this->create(
            name: $messageContext->getName(),
            subject: $messageContext->getName(),
            content: $content,
            variables: $messageContext->getVariables(),
            channels: $messageContext->getChannels(),
        );
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\JsonType;
use XOne\Bundle\NotifierBundle\Entity\ValueObject\Address;

class AddressJsonType extends JsonType
{
    public const NAME = 'x_one_notifier_address_json';

    /**
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): array
    {
        $value = parent::convertToPHPValue($value, $platform);

        if (null === $value || '' === $value) {
            return [];
        }

        return array_map(
            static function (array $data) {
                $address = new Address();
                $address->setAddress($data['address'] ?? null);
                $address->setName($data['name'] ?? null);

                return $address;
            },
            $value,
        );
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    public function getName(): string
    {
        return self::NAME;
    }
}

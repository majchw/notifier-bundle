<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use XOne\Bundle\NotifierBundle\Model\MessageContextInterface;

class MessageContext implements MessageContextInterface, \Stringable
{
    protected ?int $id = null;
    protected ?string $symbol = null;
    protected ?string $name = null;
    protected array $variables = [];
    protected array $channels = [];
    protected Collection $messageTemplates;

    public function __construct()
    {
        $this->messageTemplates = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(?string $symbol): static
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getVariables(): array
    {
        return $this->variables;
    }

    public function setVariables(array $variables): static
    {
        $this->variables = $variables;

        return $this;
    }

    public function getChannels(): array
    {
        return $this->channels;
    }

    public function setChannels(array $channels): static
    {
        $this->channels = $channels;

        return $this;
    }

    public function getMessageTemplates(): Collection
    {
        return $this->messageTemplates;
    }

    public function addMessageTemplate(MessageTemplate $messageTemplate): static
    {
        if (!$this->messageTemplates->contains($messageTemplate)) {
            $messageTemplate->setMessageContext($this);
            $this->messageTemplates->add($messageTemplate);
        }

        return $this;
    }

    public function removeMessageTemplate(MessageTemplate $messageTemplate): static
    {
        if ($this->messageTemplates->removeElement($messageTemplate)) {
            // set the owning side to null (unless already changed)
            if ($messageTemplate->getMessageContext() === $this) {
                $messageTemplate->setMessageContext(null);
            }
        }

        return $this;
    }
}

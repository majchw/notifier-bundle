<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Entity\ValueObject;

use Symfony\Component\Mime\Address as MimeAddress;
use Symfony\Component\Notifier\Recipient\Recipient;
use Symfony\Component\Notifier\Recipient\RecipientInterface;
use XOne\Bundle\NotifierBundle\Model\ValueObject\AddressInterface;

class Address implements AddressInterface
{
    private ?string $address;
    private ?string $name;

    public function __construct(string $address = null, string $name = null)
    {
        $this->address = $address;
        $this->name = $name;
    }

    public function __toString(): string
    {
        if ($this->address && $this->name) {
            return sprintf('%s <%s>', $this->name, $this->address);
        }

        return $this->address ?? $this->name ?? '';
    }

    public function jsonSerialize(): array
    {
        return array_filter([
            'address' => $this->address,
            'name' => $this->name,
        ]);
    }

    public static function fromMimeAddress(MimeAddress $mimeAddress): self
    {
        return new self($mimeAddress->getAddress(), $mimeAddress->getName());
    }

    public function toMimeAddress(): MimeAddress
    {
        return new MimeAddress($this->address, $this->name ?? '');
    }

    public function toNotifierRecipient(): RecipientInterface
    {
        return new Recipient($this->address, $this->address);
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Entity;

use XOne\Bundle\NotifierBundle\Model\MessageTemplateInterface;

class MessageTemplate implements MessageTemplateInterface, \Stringable
{
    protected ?int $id = null;
    protected ?string $name = null;
    protected ?string $subject = null;
    protected ?string $content = null;
    protected array $variables = [];
    protected array $channels = [];
    protected ?MessageContext $messageContext = null;

    public function __toString(): string
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): static
    {
        $this->subject = $subject;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): static
    {
        $this->content = $content;

        return $this;
    }

    public function getMessageContext(): ?MessageContext
    {
        return $this->messageContext;
    }

    public function setMessageContext(?MessageContext $messageContext): static
    {
        $this->messageContext = $messageContext;

        return $this;
    }

    public function getVariables(): array
    {
        return $this->variables;
    }

    public function setVariables(array $variables): static
    {
        $this->variables = $variables;

        return $this;
    }

    public function getChannels(): array
    {
        return $this->channels;
    }

    public function setChannels(array $channels): static
    {
        $this->channels = $channels;

        return $this;
    }
}

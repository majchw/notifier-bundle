<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Notifier\Message\MessageOptionsInterface;
use XOne\Bundle\NotifierBundle\Entity\ValueObject\Address;
use XOne\Bundle\NotifierBundle\Message\MessageOptions;
use XOne\Bundle\NotifierBundle\Model\MessageAttachmentInterface;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Model\MessageTransactionInterface;
use XOne\Bundle\NotifierBundle\Model\ValueObject\AddressInterface;

class Message implements MessageInterface
{
    protected ?int $id = null;
    protected ?string $subject = null;
    protected ?string $content = null;
    protected ?string $channel = null;
    protected ?string $transport = null;
    protected array $from = [];
    protected array $to = [];
    protected array $cc = [];
    protected array $bcc = [];
    protected array $replyTo = [];
    protected AddressInterface $sender;
    protected AddressInterface $returnPath;
    protected ?int $priority = null;
    protected array $options = [];
    protected ?string $recipientId = null;
    protected Collection $messageAttachments;
    protected Collection $messageTransactions;

    public function __construct()
    {
        $this->sender = new Address();
        $this->returnPath = new Address();
        $this->messageAttachments = new ArrayCollection();
        $this->messageTransactions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): static
    {
        $this->subject = $subject;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): static
    {
        $this->content = $content;

        return $this;
    }

    public function getChannel(): ?string
    {
        return $this->channel;
    }

    public function setChannel(?string $channel): static
    {
        $this->channel = $channel;

        return $this;
    }

    public function getTransport(): ?string
    {
        return $this->transport;
    }

    public function setTransport(?string $transport): static
    {
        $this->transport = $transport;

        return $this;
    }

    public function getFrom(): array
    {
        return $this->from;
    }

    public function setFrom(array $from): static
    {
        $this->from = [];

        foreach ($from as $address) {
            $this->addFrom($address);
        }

        return $this;
    }

    public function addFrom(AddressInterface $address): static
    {
        $this->from[] = $address;

        return $this;
    }

    public function getTo(): array
    {
        return $this->to;
    }

    public function setTo(array $to): static
    {
        $this->to = [];

        foreach ($to as $address) {
            $this->addTo($address);
        }

        return $this;
    }

    public function addTo(AddressInterface $address): static
    {
        $this->to[] = $address;

        return $this;
    }

    public function getCc(): array
    {
        return $this->cc;
    }

    public function setCc(array $cc): static
    {
        $this->cc = [];

        foreach ($cc as $address) {
            $this->addCc($address);
        }

        return $this;
    }

    public function addCc(AddressInterface $address): static
    {
        $this->cc[] = $address;

        return $this;
    }

    public function getBcc(): array
    {
        return $this->bcc;
    }

    public function setBcc(array $bcc): static
    {
        $this->bcc = [];

        foreach ($bcc as $address) {
            $this->addBcc($address);
        }

        return $this;
    }

    public function addBcc(AddressInterface $address): static
    {
        $this->bcc[] = $address;

        return $this;
    }

    public function getReplyTo(): array
    {
        return $this->replyTo;
    }

    public function setReplyTo(array $replyTo): static
    {
        foreach ($replyTo as $address) {
            $this->addReplyTo($address);
        }

        return $this;
    }

    public function addReplyTo(AddressInterface $address): static
    {
        $this->replyTo[] = $address;

        return $this;
    }

    public function getSender(): ?AddressInterface
    {
        // Doctrine embedded objects cannot be null.

        if (empty($this->sender->getAddress()) && empty($this->sender->getName())) {
            return null;
        }

        return $this->sender;
    }

    public function setSender(?AddressInterface $sender): void
    {
        if (null === $sender) {
            $this->sender->setName(null);
            $this->sender->setAddress(null);
        } else {
            $this->sender->setName($sender->getName());
            $this->sender->setAddress($sender->getAddress());
        }
    }

    public function getReturnPath(): ?AddressInterface
    {
        // Doctrine embedded objects cannot be null.

        if (empty($this->returnPath->getAddress()) && empty($this->returnPath->getName())) {
            return null;
        }

        return $this->returnPath;
    }

    public function setReturnPath(?AddressInterface $returnPath): void
    {
        if (null === $returnPath) {
            $this->returnPath->setName(null);
            $this->returnPath->setAddress(null);
        } else {
            $this->returnPath->setName($returnPath->getName());
            $this->returnPath->setAddress($returnPath->getAddress());
        }
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(?int $priority): void
    {
        $this->priority = $priority;
    }

    public function getOptions(): MessageOptionsInterface
    {
        return new MessageOptions($this->options, $this->recipientId);
    }

    public function setOptions(MessageOptionsInterface $options): static
    {
        $this->options = $options->toArray();
        $this->recipientId = $options->getRecipientId();

        return $this;
    }

    public function getRecipientId(): ?string
    {
        return $this->recipientId;
    }

    public function setRecipientId(?string $recipientId): void
    {
        $this->recipientId = $recipientId;
    }

    /**
     * @return Collection<MessageAttachmentInterface>
     */
    public function getMessageAttachments(): Collection
    {
        return $this->messageAttachments;
    }

    public function addMessageAttachment(MessageAttachmentInterface $messageAttachment): static
    {
        if (!$this->messageAttachments->contains($messageAttachment)) {
            $this->messageAttachments[] = $messageAttachment;
            $messageAttachment->setMessage($this);
        }

        return $this;
    }

    public function removeMessageAttachment(MessageAttachmentInterface $messageAttachment): static
    {
        $this->messageAttachments->removeElement($messageAttachment);

        return $this;
    }

    /**
     * @return Collection<MessageTransactionInterface>
     */
    public function getMessageTransactions(): Collection
    {
        return $this->messageTransactions;
    }

    public function addMessageTransaction(MessageTransactionInterface $messageTransaction): static
    {
        if (!$this->messageTransactions->contains($messageTransaction)) {
            $this->messageTransactions[] = $messageTransaction;
            $messageTransaction->setMessage($this);
        }

        return $this;
    }

    public function removeMessageTransaction(MessageTransactionInterface $messageTransaction): static
    {
        $this->messageTransactions->removeElement($messageTransaction);

        return $this;
    }
}

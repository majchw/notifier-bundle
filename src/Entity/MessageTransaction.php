<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Entity;

use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Model\MessageTransactionInterface;

class MessageTransaction implements MessageTransactionInterface
{
    protected ?int $id = null;
    protected ?MessageInterface $message = null;
    protected bool $successful = true;
    protected ?string $transportMessageId = null;
    protected ?string $exception = null;
    protected ?string $debug = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?MessageInterface
    {
        return $this->message;
    }

    public function setMessage(?MessageInterface $message): static
    {
        $this->message = $message;

        return $this;
    }

    public function isSuccessful(): bool
    {
        return $this->successful;
    }

    public function setSuccessful(bool $successful): static
    {
        $this->successful = $successful;

        return $this;
    }

    public function getTransportMessageId(): ?string
    {
        return $this->transportMessageId;
    }

    public function setTransportMessageId(?string $transportMessageId): static
    {
        $this->transportMessageId = $transportMessageId;

        return $this;
    }

    public function getException(): ?string
    {
        return $this->exception;
    }

    public function setException(?string $exception): static
    {
        $this->exception = $exception;

        return $this;
    }

    public function getDebug(): ?string
    {
        return $this->debug;
    }

    public function setDebug(?string $debug): static
    {
        $this->debug = $debug;

        return $this;
    }
}

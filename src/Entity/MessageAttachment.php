<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Entity;

use Symfony\Component\HttpFoundation\File\File;
use XOne\Bundle\NotifierBundle\Model\MessageAttachmentInterface;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;

class MessageAttachment implements MessageAttachmentInterface
{
    protected ?int $id = null;
    protected ?MessageInterface $message = null;
    protected ?string $name = null;
    protected ?string $filename = null;
    protected ?string $contentType = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): static
    {
        $this->id = $id;

        return $this;
    }

    public function getMessage(): ?MessageInterface
    {
        return $this->message;
    }

    public function setMessage(?MessageInterface $message): static
    {
        $this->message = $message;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(?string $filename): static
    {
        $this->filename = $filename;

        return $this;
    }

    public function getFile(): ?File
    {
        if ($this->filename) {
            return new File($this->filename);
        }

        return null;
    }

    public function getContentType(): ?string
    {
        return $this->contentType;
    }

    public function setContentType(?string $contentType): static
    {
        $this->contentType = $contentType;

        return $this;
    }
}

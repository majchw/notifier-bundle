<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Mailer\Event\FailedMessageEvent as MailerFailedMessageEvent;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Notifier\Event\FailedMessageEvent as NotifierFailedMessageEvent;
use XOne\Bundle\NotifierBundle\Factory\MessageTransactionFactoryInterface;
use XOne\Bundle\NotifierBundle\PersistentMessageAwareInterface;
use XOne\Bundle\NotifierBundle\Repository\MessageRepositoryInterface;
use XOne\Bundle\NotifierBundle\Repository\MessageTransactionRepositoryInterface;

class FailedMessageSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly MessageRepositoryInterface $messageRepository,
        private readonly MessageTransactionRepositoryInterface $messageTransactionRepository,
        private readonly MessageTransactionFactoryInterface $messageTransactionFactory,
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            MailerFailedMessageEvent::class => 'createMessageTransactionFromFailedMessage',
            NotifierFailedMessageEvent::class => 'createMessageTransactionFromFailedMessage',
            ExceptionEvent::class => 'createMessageTransactionFromFailedHandler',
        ];
    }

    public function createMessageTransactionFromFailedMessage(MailerFailedMessageEvent|NotifierFailedMessageEvent $event): void
    {
        $this->createMessageTransaction($event->getMessage(), $event->getError());
    }

    public function createMessageTransactionFromFailedHandler(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if (!$exception instanceof HandlerFailedException) {
            return;
        }

        $this->createMessageTransaction($exception->getEnvelope()->getMessage(), $exception);
    }

    public function createMessageTransaction(mixed $subject, \Throwable $exception): void
    {
        if (!$subject instanceof PersistentMessageAwareInterface) {
            return;
        }

        $persistentMessageId = $subject->getPersistentMessageId();

        if (null === $persistentMessageId) {
            return;
        }

        $message = $this->messageRepository->find($persistentMessageId);

        if (null === $message) {
            return;
        }

        $messageTransaction = $this->messageTransactionFactory->createFailed($message, $exception);

        $this->messageTransactionRepository->save($messageTransaction);
    }
}

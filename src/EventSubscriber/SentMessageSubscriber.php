<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Event\SentMessageEvent as MailerSentMessageEvent;
use Symfony\Component\Mailer\SentMessage as MailerSentMessage;
use Symfony\Component\Notifier\Event\SentMessageEvent as NotifierSentMessageEvent;
use Symfony\Component\Notifier\Message\SentMessage as NotifierSentMessage;
use XOne\Bundle\NotifierBundle\Entity\ValueObject\Address;
use XOne\Bundle\NotifierBundle\Factory\MessageTransactionFactoryInterface;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\PersistentMessageAwareInterface;
use XOne\Bundle\NotifierBundle\Repository\MessageRepositoryInterface;
use XOne\Bundle\NotifierBundle\Repository\MessageTransactionRepositoryInterface;

class SentMessageSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly MessageRepositoryInterface $messageRepository,
        private readonly MessageTransactionRepositoryInterface $messageTransactionRepository,
        private readonly MessageTransactionFactoryInterface $messageTransactionFactory,
        private readonly bool $debug = true,
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            MailerSentMessageEvent::class => [
                ['createMessageTransaction'],
                ['updateFromAddress'],
            ],
            NotifierSentMessageEvent::class => 'createMessageTransaction',
        ];
    }

    public function createMessageTransaction(MailerSentMessageEvent|NotifierSentMessageEvent $event): void
    {
        $sentMessage = $event->getMessage();
        $message = $this->getPersistentMessageFromSentMessage($sentMessage);

        if (null === $message) {
            return;
        }

        $messageTransaction = $this->messageTransactionFactory->createSuccessful(
            message: $message,
            transportMessageId: $sentMessage->getMessageId(),
        );

        if ($this->debug && $sentMessage instanceof MailerSentMessage) {
            $messageTransaction->setDebug($sentMessage->getDebug());
        }

        $this->messageTransactionRepository->save($messageTransaction);
    }

    public function updateFromAddress(MailerSentMessageEvent $event): void
    {
        $sentMessage = $event->getMessage();
        $message = $this->getPersistentMessageFromSentMessage($sentMessage);

        if (null === $message || !empty($message->getFrom())) {
            return;
        }

        $message->addFrom(Address::fromMimeAddress($sentMessage->getEnvelope()->getSender()));

        $this->messageRepository->save($message);
    }

    private function getPersistentMessageFromSentMessage(MailerSentMessage|NotifierSentMessage $sentMessage): ?MessageInterface
    {
        $subject = match (true) {
            is_a($sentMessage, MailerSentMessage::class) => $sentMessage->getEnvelope(),
            is_a($sentMessage, NotifierSentMessage::class) => $sentMessage->getOriginalMessage(),
            default => null,
        };

        if ($subject instanceof PersistentMessageAwareInterface) {
            $persistentMessageId = $subject->getPersistentMessageId();

            if ($persistentMessageId) {
                return $this->messageRepository->find($persistentMessageId);
            }
        }

        return null;
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Message;

use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Notifier\Message\EmailMessage as BaseEmailMessage;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;
use XOne\Bundle\NotifierBundle\Mailer\PersistentMessageEmail;
use XOne\Bundle\NotifierBundle\Mailer\PersistentMessageEnvelope;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Model\ValueObject\AddressInterface;
use XOne\Bundle\NotifierBundle\Notification\PersistentMessageNotification;
use XOne\Bundle\NotifierBundle\PersistentMessageAwareInterface;
use XOne\Bundle\NotifierBundle\PersistentMessageAwareTrait;

/**
 * Extension of the {@see BaseEmailMessage} to work with the bundle's persistent {@see MessageInterface}.
 * Every email message should be sent using this class to properly handle the persistence-related logic.
 *
 * @method PersistentMessageEmail getMessage()
 */
class EmailMessage extends BaseEmailMessage implements PersistentMessageAwareInterface
{
    use NotificationAwareTrait;
    use PersistentMessageAwareTrait;

    public function __construct(PersistentMessageEmail $message, Envelope $envelope = null)
    {
        parent::__construct($message, $envelope);
    }

    public static function fromNotification(Notification $notification, EmailRecipientInterface $recipient, string $transport = null): self
    {
        $email = (new PersistentMessageEmail())
            ->subject($notification->getSubject())
            ->htmlTemplate('@XOneNotifier/Email/body.html.twig')
            ->context([
                'content' => $notification->getContent(),
            ])
        ;

        $message = new self($email);
        $message->transport($transport);
        $message->notification = $notification;

        if ($notification instanceof PersistentMessageNotification) {
            $email->updateFromPersistentMessage($notification->getPersistentMessage());
            $message->updateFromPersistentMessage($notification->getPersistentMessage());
        }

        return $message;
    }

    public function updateFromPersistentMessage(MessageInterface $persistentMessage): void
    {
        $email = $this->getMessage();

        if ($from = $persistentMessage->getFrom()) {
            $email->from(...array_map(fn (AddressInterface $address) => $address->toMimeAddress(), $from));
        }

        if ($to = $persistentMessage->getTo()) {
            $email->to(...array_map(fn (AddressInterface $address) => $address->toMimeAddress(), $to));
        }

        if ($cc = $persistentMessage->getCc()) {
            $email->cc(...array_map(fn (AddressInterface $address) => $address->toMimeAddress(), $cc));
        }

        if ($bcc = $persistentMessage->getBcc()) {
            $email->bcc(...array_map(fn (AddressInterface $address) => $address->toMimeAddress(), $bcc));
        }

        if ($replyTo = $persistentMessage->getReplyTo()) {
            $email->replyTo(...array_map(fn (AddressInterface $address) => $address->toMimeAddress(), $replyTo));
        }

        if ($sender = $persistentMessage->getSender()) {
            $email->sender($sender->toMimeAddress());
        }

        if ($returnPath = $persistentMessage->getReturnPath()) {
            $email->returnPath($returnPath->toMimeAddress());
        }

        if (null !== $priority = $persistentMessage->getPriority()) {
            $email->priority($priority);
        }

        foreach ($persistentMessage->getMessageAttachments() as $messageAttachment) {
            if (null === $path = $messageAttachment->getFilename()) {
                continue;
            }

            $email->attachFromPath($path, $messageAttachment->getName(), $messageAttachment->getContentType());
        }

        $this->persistentMessageId = $persistentMessage->getId();

        $this->transport($persistentMessage->getTransport());

        $envelope = new PersistentMessageEnvelope($persistentMessage->getId(), $this->getEnvelope());

        if (empty($recipients = $envelope->getOriginalEnvelope()?->getRecipients() ?? [])) {
            $recipients = array_merge($email->getTo(), $email->getCc(), $email->getBcc());
        }

        $envelope->setRecipients($recipients);

        $this->envelope($envelope);
    }
}

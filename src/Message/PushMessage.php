<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Message;

use Symfony\Component\Notifier\Message\PushMessage as BasePushMessage;
use Symfony\Component\Notifier\Notification\Notification;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Notification\PersistentMessageNotification;
use XOne\Bundle\NotifierBundle\PersistentMessageAwareInterface;
use XOne\Bundle\NotifierBundle\PersistentMessageAwareTrait;

/**
 * Extension of the {@see BasePushMessage} to work with the bundle's persistent {@see MessageInterface}.
 * Every push message should be sent using this class to properly handle the persistence-related logic.
 */
class PushMessage extends BasePushMessage implements PersistentMessageAwareInterface
{
    use NotificationAwareTrait;
    use PersistentMessageAwareTrait;

    public static function fromNotification(Notification $notification, string $transport = null): self
    {
        $message = new self($notification->getSubject(), $notification->getContent());
        $message->transport($transport);
        $message->notification = $notification;

        if ($notification instanceof PersistentMessageNotification) {
            $message->updateFromPersistentMessage($notification->getPersistentMessage());
        }

        return $message;
    }

    public function updateFromPersistentMessage(MessageInterface $persistentMessage): void
    {
        $this->persistentMessageId = $persistentMessage->getId();

        $this->transport($persistentMessage->getTransport());
        $this->options($persistentMessage->getOptions());
    }
}

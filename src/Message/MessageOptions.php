<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Message;

use Symfony\Component\Notifier\Message\MessageOptionsInterface;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;

/**
 * Simple, generic implementation of the {@see MessageOptionsInterface} to deserialize options to.
 *
 * Doesn't matter which implementation of the {@see MessageOptionsInterface} is set to the {@see MessageInterface},
 * the serialization process (json) loses information of the original class, and deserialization results in this class.
 */
class MessageOptions implements MessageOptionsInterface
{
    public function __construct(
        private readonly array $options = [],
        private readonly ?string $recipientId = null
    ) {
    }

    public function toArray(): array
    {
        return $this->options;
    }

    public function getRecipientId(): ?string
    {
        return $this->recipientId;
    }
}

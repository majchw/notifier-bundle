<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Message;

use Symfony\Component\Notifier\Notification\Notification;

trait NotificationAwareTrait
{
    private ?Notification $notification = null;

    public function getNotification(): ?Notification
    {
        return $this->notification;
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Message;

use Symfony\Component\Notifier\Message\SmsMessage as BaseSmsMessage;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\SmsRecipientInterface;
use XOne\Bundle\NotifierBundle\Exception\LogicException;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Notification\PersistentMessageNotification;
use XOne\Bundle\NotifierBundle\PersistentMessageAwareInterface;
use XOne\Bundle\NotifierBundle\PersistentMessageAwareTrait;

/**
 * Extension of the {@see BaseSmsMessage} to work with the bundle's persistent {@see MessageInterface}.
 * Every SMS message should be sent using this class to properly handle the persistence-related logic.
 */
class SmsMessage extends BaseSmsMessage implements PersistentMessageAwareInterface
{
    use NotificationAwareTrait;
    use PersistentMessageAwareTrait;

    public static function fromNotification(Notification $notification, SmsRecipientInterface $recipient, string $transport = null): self
    {
        $message = new self($recipient->getPhone(), $notification->getSubject());
        $message->transport($transport);
        $message->notification = $notification;

        if ($notification instanceof PersistentMessageNotification) {
            $message->updateFromPersistentMessage($notification->getPersistentMessage());
        }

        return $message;
    }

    public function updateFromPersistentMessage(MessageInterface $persistentMessage): void
    {
        $this->persistentMessageId = $persistentMessage->getId();

        if ($from = $persistentMessage->getFrom()) {
            if (count($from) > 1) {
                throw new LogicException('The SMS message can only have one sender.');
            }

            $this->from(current($from)->getAddress());
        }

        $this->transport($persistentMessage->getTransport());

        // Backwards compatibility layer for Notifier < 6.3
        if (method_exists($this, 'options')) {
            $this->options($persistentMessage->getOptions());
        }
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Sender;

use Symfony\Component\Notifier\NotifierInterface;
use XOne\Bundle\NotifierBundle\Exception\LogicException;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Model\ValueObject\AddressInterface;
use XOne\Bundle\NotifierBundle\Notification\PersistentMessageNotification;
use XOne\Bundle\NotifierBundle\Recipient\NullRecipient;
use XOne\Bundle\NotifierBundle\Repository\MessageRepositoryInterface;

class NotifierMessageSender implements MessageSenderInterface
{
    public function __construct(
        private readonly NotifierInterface $notifier,
        private readonly MessageRepositoryInterface $messageRepository,
    ) {
    }

    public function send(MessageInterface $message): void
    {
        if (null === $channel = $message->getChannel()) {
            throw new LogicException('Attempting to send a message without channel specified.');
        }

        $notification = new PersistentMessageNotification($message);

        // Message has to be saved first, so its identifier can be used in the notification.
        // This shouldn't matter as the message can be sent asynchronously anyway.
        $this->messageRepository->save($message);

        $recipients = array_map(
            fn (AddressInterface $address) => $address->toNotifierRecipient(),
            $message->getTo(),
        );

        // https://bitbucket.org/majchw/notifier-bundle/src/master/docs/null-recipient.md
        if ('email' === $channel && count($recipients) > 0) {
            $recipients = [new NullRecipient()];
        }

        $this->notifier->send($notification, ...$recipients);
    }
}

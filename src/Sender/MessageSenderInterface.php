<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Sender;

use XOne\Bundle\NotifierBundle\Model\MessageInterface;

interface MessageSenderInterface
{
    public function send(MessageInterface $message): void;
}

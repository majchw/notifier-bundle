<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Sender;

use Symfony\Contracts\Service\Attribute\Required;
use XOne\Bundle\NotifierBundle\Exception\LogicException;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;

trait MessageSenderAwareTrait
{
    private ?MessageSenderInterface $messageSender = null;

    #[Required]
    public function setMessageSender(MessageSenderInterface $messageSender): void
    {
        $this->messageSender = $messageSender;
    }

    protected function send(MessageInterface $message): void
    {
        if (null === $this->messageSender) {
            $error = sprintf('You cannot use the "%s" method, because the message sender is not available.', __METHOD__);
            $error .= ' Make sure the "setMessageSender()" method is called (e.g. by the container via setter injection).';

            throw new LogicException($error);
        }

        $this->messageSender->send($message);
    }
}

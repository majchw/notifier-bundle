# NotifierBundle

Paczka integrująca [Symfony Notifier](https://symfony.com/doc/current/notifier.html), ułatwiająca tworzenie powiadomień (wiadomości)
oraz oferująca dodatkowe funkcjonalności takie jak szablony wiadomości, oraz historię ich wysyłki.

- [Instalacja](docs/installation.md)
- [Konfiguracja](docs/configuration.md)
- [Instrukcja wykorzystania](docs/usage.md)
- [Zmienne w szablonach](docs/variables.md)
- [Zastosowanie i specyfika `NullRecipient`](docs/null-recipient.md)
- [Rozwiązywanie problemów](docs/troubleshooting.md)
- [Rozwój bundle](docs/contributing.md)

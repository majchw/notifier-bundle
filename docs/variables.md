# Zmienne w szablonach

Szablony posiadać mogą zmienne w swoim temacie oraz treści, np.:

```
Cena produktu {{ product_name }} została zmieniona na {{ product_price }} zł.
```

Powyższy przykładowy szablon wymaga dwóch zmiennych:

- `product_name` (nazwa produktu)
- `product_price` (cena produktu)

Nazwy tych zmiennych mogą, ale nie muszą (jak w powyższym przykładzie) odzwierciedlać nazw pól danej encji.
Sama wysyłka może nawet nie dotyczyć konkretnej encji, dlatego rozwiązania są dwa, a docelowe powinno zostać wybrane **zależnie od sytuacji**.

## Przekazanie tablicy danych

Najprostszą opcją jest przekazanie tablicy z danymi wykorzystywanymi w szablonie:

```php
use App\Entity\Product\Product;
use XOne\Bundle\NotifierBundle\Factory\MessageFactoryInterface;
use XOne\Bundle\NotifierBundle\Model\MessageTemplateInterface;

/** @var Product $product */
/** @var MessageFactoryInterface $messageFactory */
/** @var MessageTemplateInterface $messageTemplate */

$message = $messageFactory->createTemplated($messageTemplate, [
    'product_name' => $product->getName(),
    'product_price' => $product->getPrice(),
]);
```

Wykorzystany zostanie [`PropertyAccessor`](https://symfony.com/doc/current/components/property_access.html) który na podstawie nazwy zmiennej (np. `product_name`)
pobierze wartość z tablicy. Odpowiedzialny jest za to [wbudowany provider `PropertyAccessorVariableValueProvider`](../src/Templating/Variables/PropertyAccessorVariableValueProvider.php).

## Utworzenie DTO dla wysyłki szablonu

Jedną z opcji jest utworzenie obiektu, który posiada gettery odpowiadające zmiennym w szablonie.
Obiekt DTO tworzony jest ręcznie (przez programistę), dlatego może przyjmować w konstruktorze
dowolne parametry (np. encje, których dotyczy wiadomość):

```php
namespace App\Notifier\Templating\Context\Product;

use App\Entity\Product\Product;

readonly class ProductContext
{
    public function __construct(
        private Product $product,
    ) {
    }

    public function getProductName(): string
    {
        return $this->product->getName();
    }

    public function getProductPrice(): float
    {
        return $this->product->getPrice();
    }
}
```

Obiekt DTO może zostać przekazany jako `subject` podczas wysyłki wiadomości:

```php
use App\Entity\Product\Product;
use App\Notifier\Templating\Context\Product\ProductContext;
use XOne\Bundle\NotifierBundle\Factory\MessageFactoryInterface;
use XOne\Bundle\NotifierBundle\Model\MessageTemplateInterface;

/** @var Product $product */
/** @var MessageFactoryInterface $messageFactory */
/** @var MessageTemplateInterface $messageTemplate */

$message = $messageFactory->createTemplated($messageTemplate, new ProductContext($product));
```

Wykorzystany zostanie [`PropertyAccessor`](https://symfony.com/doc/current/components/property_access.html) który na podstawie nazwy zmiennej (np. `product_name`)
uruchomi odpowiedni getter w DTO (np. `getProductName()`). Odpowiedzialny jest za to [wbudowany provider `PropertyAccessorVariableValueProvider`](../src/Templating/Variables/PropertyAccessorVariableValueProvider.php).

## Utworzenie `VariableValueProvider` dla produktu

Jest to alternatywna opcja do DTO, której plusem jest to, że dla jednej encji możemy utworzyć wiele providerów, podczas gdy DTO jest jedno.

```php
namespace App\Notifier\Templating;

use App\Entity\Product\Product;
use XOne\Bundle\NotifierBundle\Exception\VariableValueProviderException;
use XOne\Bundle\NotifierBundle\Templating\Variables\VariableValueProviderInterface;

class ProductVariableValueProvider implements VariableValueProviderInterface
{
    public function getValue(string $name, mixed $subject = null): mixed
    {
        if (!$subject instanceof Product) {
            throw new VariableValueProviderException();
        }

        return match ($name) {
            case 'product_name' => $subject->getName(),
            case 'product_price' => $subject->getPrice(),
            default: throw new VariableValueProviderException(),
        }
    }
}
```

Tak utworzonej klasy nie musimy dodatkowo konfigurować, ponieważ automatycznie zostanie zarejestrowana w kontenerze.

**Uwaga**: jeśli provider nie jest w stanie zwrócić wartości dla danej zmiennej i obiektu,
powinien wyrzucić [wyjątek `VariableValueProviderException`](../src/Exception/VariableValueProviderException.php). Dzięki temu system będzie wiedzieć,
że powinien kontynuować i przejść przez pozostałe (zarejestrowane w kontenerze) providery.

W takiej konfiguracji obiekt DTO nie jest potrzebny i wystarczy przekazać bezpośrednio encję:

```php
use App\Entity\Product\Product;
use XOne\Bundle\NotifierBundle\Factory\MessageFactoryInterface;
use XOne\Bundle\NotifierBundle\Model\MessageTemplateInterface;

/** @var Product $product */
/** @var MessageFactoryInterface $messageFactory */
/** @var MessageTemplateInterface $messageTemplate */

$message = $messageFactory->createTemplated($messageTemplate, $product);
```

## Dodawanie globalnych zmiennych

Załóżmy, że w każdym mailu potrzebujemy dodać zmienną `application_user`, która reprezentuje zalogowanego użytkownika.
Zamiast dodawać to do każdego DTO lub providera dla konkretnej encji, możemy utworzyć osobny provider:

```php
namespace App\Notifier\Templating;

use XOne\Bundle\NotifierBundle\Exception\VariableValueProviderException;
use XOne\Bundle\NotifierBundle\Templating\Variables\VariableValueProviderInterface;

class AppUserVariableValueProvider implements VariableValueProviderInterface
{
    public function getValue(string $name, mixed $subject = null): mixed
    {
        if ('application_user' !== $name) {
            throw new VariableValueProviderException();
        }

        return $this->security->getUser()?->getUserIdentfier();
    }
}
```

## Kolejność providerów

W bardzo złożonych systemach kolejność wywoływania providerów może mieć znaczenie.
Kolejność providerów determinuje ich [priorytet w definicji w kontenerze](https://symfony.com/doc/current/service_container/tags.html#tagged-services-with-priority).

Załóżmy, że mamy dwa providery:

- `App\Notifier\Templating\FirstVariableValueProvider`
- `App\Notifier\Templating\SecondVariableValueProvider`

Jeśli chcemy, aby `FirstVariableValueProvider` był uruchamiany przed `SecondVariableValueProvider`,
możemy w konfiguracji serwisów ustalić ich priorytet (im wyższy, tym wcześniej zostanie wywołany):

```yaml
# config/services.yaml

App\Notifier\Templating\FirstVariableValueProvider:
  tags:
    - { name: 'x_one_notifier.variable_value_provider', priority: 1 }

App\Notifier\Templating\SecondVariableValueProvider:
  tags:
    - { name: 'x_one_notifier.variable_value_provider', priority: 2 }
```

Alternatywną opcją niewymagającą zmian w konfiguracji serwisów jest dodanie metody statycznej `getDefaultPriority()`:

```php
namespace App\Notifier\Templating;

use XOne\Bundle\NotifierBundle\Templating\Variables\VariableValueProviderInterface;

class FirstVariableValueProvider implements VariableValueProviderInterface
{
    public static function getDefaultPriority(): int
    {
        return 2;
    }
}

class SecondVariableValueProvider implements VariableValueProviderInterface
{
    public static function getDefaultPriority(): int
    {
        return 1;
    }
}
```



# Rozwój bundle

## Testy

Upewnij się, że testy nie zwracają żadnego błędu:

```bash
vendor/bin/simple-phpunit
```

## Quality control

Upewnij się, że kod jest sformatowany poprawnie (dzięki php-cs-fixer) oraz że PHPStan nie zwraca żadnych błędów:

```bash
vendor/bin/php-cs-fixer fix
vendor/bin/phpstan analyze
```

## Wersjonowanie

Aby paczkę dało się zaktualizować przez composera, po zmergowaniu zmian do głównego brancha, należy utworzyć tag w formacie `vX.Y.Z`, np.

```bash
git tag -a v1.1.0 -m "Version v1.1.0"
git push --tags
```

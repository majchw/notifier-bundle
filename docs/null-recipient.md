# Zastosowanie i specyfika `NullRecipient`

`NullRecipient` jest klasą która "udaje" odbiorcę powiadomienia (notification) podobnie do wbudowanego `NoRecipient`,
lecz dodatkowo implementuje interfejsy wymagane do obsługi przez kanały e-mail oraz SMS.

## Dlaczego nie przekazujemy odbiorców "normalnie"?

Załóżmy, że w klasie `MessageSender` wysyłalibyśmy wiadomość e-mail w takowy sposób:

```php
use XOne\Bundle\NotifierBundle\Entity\Message;
use XOne\Bundle\NotifierBundle\Entity\ValueObject\Address;

$message = new Message();
$message->addTo(new Address('recipient1@example.com'));
$message->addTo(new Address('recipient2@example.com'));

// ...

$notification = new PersistentMessageNotification($message);

$this->notifier->send($notification, ...array_map(
    fn (Address $address) => $address->toNotifierRecipient(),
    $message->getTo(),
));
```

W takim scenariuszu mamy obiekt wiadomości (`Message`) która ma dwóch odbiorców:

- `recipient1@example.com`
- `recipient2@example.com`

Tych odbiorców przekazujemy jako odbiorców powiadomienia do metody `send()` Notifiera.

Bundle na podstawie odbiorców wiadomości tworzy e-mail do wysłania:

```php
/** @var \XOne\Bundle\NotifierBundle\Model\MessageInterface $message */

if ($to = $message->getTo()) {
    $email->to(...array_map(fn (AddressInterface $address) => $address->toMimeAddress(), $to));
}
```

Ponieważ przekazaliśmy dwóch odbiorców do Notifiera, tak utworzona wiadomość wyjdzie do obu odbiorców,
co oznacza, że obie wiadomości będą posiadać dwóch odbiorców jako "TO". W rzeczywistości wiąże się to z tym,
że wyjdą cztery maile. W przypadku trzech odbiorców — dziewięć maili, w przypadku czterech — szesnaście itd.

Aby temu zapobiec, zawsze przekazujemy do Notifiera jednego odbiorcę — naszego `NullRecipient`.
Dzięki temu powiadomienie zawsze wychodzi raz, a nasz bundle pod maską przetwarza je na maila z wieloma odbiorcami.

Dla bezpieczeństwa oba gettery w `NullRecipient` wyrzucają wyjątek z instrukcją jak poprawnie pobrać odbiorców.

## Jak prawidłowo pobrać odbiorców?

Aby mieć dostęp do odbiorców, potrzebujemy pobrać obiekt wiadomości (nasz bundlowy `MessageInterface`).

- jeżeli mamy dostęp do obiektu `XOne\Bundle\NotifierBundle\Notification\PersistentMessageNotification`, możemy użyć gettera `getPersistentMessage()`;
- jeżeli mamy dostęp do obiektu `XOne\Bundle\NotifierBundle\Mailer\PersistentMessageEmail`, możemy użyć gettera `getPersistentMessageId()` do pobrania **identyfikatora** wiadomości;
- jeżeli mamy dostęp do obiektu `XOne\Bundle\NotifierBundle\Mailer\PersistentMessageEnvelope`, możemy użyć gettera `getPersistentMessageId()` do pobrania **identyfikatora** wiadomości;

Przykład pobrania obiektu wiadomości "w praktyce" znajdziemy w event subscriberach z bundle:

- `XOne\Bundle\NotifierBundle\EventSubscriber\SentMessageSubscriber`
- `XOne\Bundle\NotifierBundle\EventSubscriber\FailedMessageSubscriber`

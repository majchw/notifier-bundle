# Instrukcja wykorzystania

**Uwaga**: ta sekcja dokumentacji zakłada, że mamy już [utworzone encje w namespace App\Entity\Notifier\...](configuration.md#encje).

## Tworzenie wiadomości

Aby utworzyć wiadomość, utwórz obiekt encji typu `Message`:

```php
use App\Entity\Notifier\Message;

$message = (new Message())
    ->setSubject('Wiadomość z formularza kontaktowego')
    ->setContent('Odnotowano nową wiadomość z poziomu formularza kontaktowego')
    ->setChannel('email');
```

Możesz też skorzystać z `MessageFactory`:

```php
use XOne\Bundle\NotifierBundle\Factory\MessageFactoryInterface;

/** @var MessageFactoryInterface $messageFactory */

$message = $messageFactory->createEmail(
    subject: 'Wiadomość z formularza kontaktowego',
    content: 'Odnotowano nową wiadomość z poziomu formularza kontaktowego',
);
```

Aby utworzyć wiadomość na podstawie szablonu `MessageTemplate`, skorzystaj z `MessageFactory`:

```php
use App\Entity\Notifier\MessageTemplate;
use XOne\Bundle\NotifierBundle\Factory\MessageFactoryInterface;

/** @var MessageFactoryInterface $messageFactory */
/** @var MessageTemplate $messageTemplate */

$message = $messageFactory->createTemplatedEmail($messageTemplate);
```

**Uwaga**: kanał (`channel`) wiadomości determinuje sposób jej wysyłki.

| Sposób wysyłki | Nazwa kanału | Metody w `MessageFactory`             |
|----------------|--------------|---------------------------------------|
| Email          | `email`      | `createEmail`, `createTemplatedEmail` |
| SMS            | `sms`        | `createSms`, `createTemplatedSms`     |
| Chat           | `chat`       | `createChat`, `createTemplatedChat`   |
| Push           | `push`       | `createPush`, `createTemplatedPush`   |

## Wysyłka wiadomości

Wiadomości wysyłamy za pomocą `MessageSender`:

```php
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Sender\MessageSenderInterface;

/** @var MessageSenderInterface $messageSender */
/** @var MessageInterface $message */

$messageSender->send($message);
```

Szczegóły procesu wysyłki zależą od konfiguracji w systemie.

## Konfiguracja

### Włączenie wysyłki asynchronicznej

Domyślnie wszystkie wiadomości wychodzą synchronicznie.
Możemy to zmienić w pliku konfiguracji `config/packages/messenger.yaml`:

```yaml
# config/packages/messenger.yaml
framework:
  messenger:
    routing:
      'Symfony\Component\Mailer\Messenger\SendEmailMessage': 'async'
      'XOne\Bundle\NotifierBundle\Message\SmsMessage': 'async'
      'XOne\Bundle\NotifierBundle\Message\PushMessage': 'async'
      'XOne\Bundle\NotifierBundle\Message\ChatMessage': 'async'
```

### Konfiguracja dostawców wysyłek

Za obsługę wysyłek wiadomości odpowiadają dostawcy (tzw. `transport`) - przykładowo, dostawcą wiadomości SMS może być SMSAPI.
Listę oficjalnie wspieranych dostawców znajdziemy [w oficjalnej dokumentacji](https://symfony.com/doc/current/notifier.html#channels-chatters-texters-email-browser-and-push).

Przykład instalacji i konfiguracji dostawcy SMSAPI:

```shell
composer require symfony/smsapi-notifier
```

```yaml
# config/packages/notifier.yaml
framework:
  notifier:
    texter_transports:
      smsapi: '%env(SMSAPI_DSN)%'
```

```dotenv
# .env
SMSAPI_DSN=smsapi://TOKEN@default?from=FROM
```

Jeśli posiadamy w systemie wielu skonfigurowanych dostawców, domyślnie wysyłka odbędzie się z użyciem pierwszego z listy.
Gdyby zaszła potrzeba, aby konkretnie skorzystać z danego dostawcy, jego nazwę możemy podać w samej encji wiadomości:

```php
use XOne\Bundle\NotifierBundle\Model\MessageInterface;

/** @var MessageInterface $message */

$message->setTransport('twilio');
```

### Konfiguracja "fake" wysyłki wiadomości SMS oraz chatowych

Istnieją dwie paczki umożliwiające "udawanie" wysyłki wiadomości z kanałów SMSowych oraz chatowych:

- dla wiadomości SMS — [symfony/fake-sms-notifier](https://github.com/symfony/fake-sms-notifier);
- dla wiadomości chatowych — [symfony/fake-chat-notifier](https://github.com/symfony/fake-chat-notifier);

Traktujemy ich jako dostawców, więc konfigurujemy w pliku `config/packages/notifier.yaml`:

```yaml
# config/packages/notifier.yaml
framework:
  notifier:
    texter_transports:
      # This transport will fake SMS sending by writing to the logger.
      # If you wish to send the SMS to the email instead, use:
      # fakesms: fakesms+email://default?to=TO&from=FROM
      fakesms: 'fakesms+logger://default'
    chatter_transports:
      # This transport will fake chat messages sending by writing to the logger.
      # If you wish to send the chat message to the email instead, use:
      # fakechat: fakechat+email://default?to=TO&from=FROM
      fakechat: 'fakechat+logger://default'
```

Umożliwiają one przekierowanie wysyłanych wiadomości do loggera lub na podany adres e-mail.

**Uwaga**: obie te paczki **nie** wysyłają eventów informujących system o wysyłce, przez co m.in. nie utworzą się rekordy `MessageTransaction`,
ponieważ ich domyślna implementacja nie korzysta z `EventDispatcherInterface`. Jeśli chcemy, aby powyżsi dostawcy informowali
system o wysyłce, należy dodać poniższe wpisy do `config/services.yaml`:

```yaml
# config/services.yaml
services:
  notifier.transport_factory.fake-chat:
    class: XOne\Bundle\NotifierBundle\Bridge\FakeChat\FakeChatTransportFactory

  notifier.transport_factory.fake-sms:
    class: XOne\Bundle\NotifierBundle\Bridge\FakeSms\FakeSmsTransportFactory
```

Powyższa konfiguracja nadpisze definicję serwisów obu tych dostawców na klasy wysyłające poprawnie eventy.

# Rozwiązywanie problemów

### Attempting to send a message without channel specified.

Błąd ten występuje podczas próby wysyłki wiadomości bez ustawionego kanału (np. `email`).
Kanał wiadomości możemy ustawić przez metodę `setChannel()`, lub podczas samego jej tworzenia z wykorzystaniem
odpowiedniej metody `MessageFactory`:

```php
// Ręcznie
$message = (new Message())->setChannel('email');

// Za pomocą factory
$message = $messageFactory->createEmail();
```

### The "..." channel does not exist.

Błąd ten występuje podczas próby wysyłki z wykorzystaniem kanału, który nie istnieje, lub nie jest w stanie wysłać danej wiadomości.

W pierwszej kolejności upewnij się, że wykorzystywany kanał istnieje. Domyślnie dostępne kanały to: `email`, `sms`, `chat` oraz `push`.

Następnie, upewnij się, że wykorzystywany kanał jest w stanie obsłużyć wysyłkę danej wiadomości.
Przykładowo, kanał `email` wymaga, aby wiadomość miała podanego odbiorcę.

### Handling "..." failed: The "..." transport does not exist.

Błąd ten występuje podczas próby wysyłki z wykorzystaniem dostawcy, który nie istnieje w systemie.

**Uwaga**: każdy dostawca wymaga doinstalowania osobno paczki oraz dodania odpowiedniej konfiguracji w `config/packages/notifier.yaml`, np.:

```shell
composer require symfony/smsapi-notifier
```

```yaml
# config/packages/notifier.yaml
framework:
    notifier:
        texter_transports:
            smsapi: '%env(SMSAPI_DSN)%'
```

Więcej informacji (wraz z listą oficjalnie wspieranych dostawców) [w oficjalnej dokumentacji](https://symfony.com/doc/current/notifier.html#channels-chatters-texters-email-browser-and-push).

### Handling "..." failed: None of the available transports support the given message (available transports: "...").

Błąd ten występuje, gdy próbujemy wysłać wiadomość, której nie jest w stanie wysłać żaden "transport" (czyt. dostawca).
Jeśli treść błędu zawiera pusty string jako listę dostępnych transportów, oznacza to, że nie mamy w systemie zdefiniowanego **ani jednego** transportu dla danego kanału.

Jeśli posiadamy w systemie skonfigurowane wielu dostawców dla jednego kanału (np. `smsapi` oraz `twillio`),
domyślnie wykorzystywany jest ten zdefiniowany jako pierwszy. Jeśli nie jest on w stanie obsłużyć wysyłki,
możemy ustawić, aby wiadomość korzystała z innego dostawcy przez metodę `setTransport()`:

```yaml
# config/packages/notifier.yaml
framework:
    notifier:
        texter_transports:
            smsapi: '%env(SMSAPI_DSN)%' # domyślny dostawca
            twilio: '%env(TWILIO_DSN)%'
```

```php
// wysyłka takiej wiadomości wykorzysta "smsapi" (brak ustawionego dostawcy, więc użyje pierwszego z konfiguracji)
$message = $this->messageFactory->createSms();

// od teraz wysyłka takiej wiadomości wykorzysta "twilio"
$message->setTransport('twilio');
```

**Uwaga**: każdy dostawca wymaga doinstalowania osobno paczki oraz dodania odpowiedniej konfiguracji w `config/packages/notifier.yaml`, np.:

```shell
composer require symfony/smsapi-notifier
```

```yaml
# config/packages/notifier.yaml
framework:
    notifier:
        texter_transports:
            smsapi: '%env(SMSAPI_DSN)%'
```

Więcej informacji (wraz z listą oficjalnie wspieranych dostawców) [w oficjalnej dokumentacji](https://symfony.com/doc/current/notifier.html#channels-chatters-texters-email-browser-and-push).

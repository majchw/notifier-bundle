# Konfiguracja

Paczkę konfigurujemy za pomocą pliku `config/packages/x_one_notifier.yaml`.

## Encje

Paczka wymaga dodania encji:

- `Message` rozszerzającej `XOne\Bundle\NotifierBundle\Entity\Message`
- `MessageAttachment` rozszerzającej `XOne\Bundle\NotifierBundle\Entity\MessageAttachment`
- `MessageTemplate` rozszerzającej `XOne\Bundle\NotifierBundle\Entity\MessageTemplate`
- `MessageContext` rozszerzającej `XOne\Bundle\NotifierBundle\Entity\MessageContext`
- `MessageTransaction` rozszerzającej `XOne\Bundle\NotifierBundle\Entity\MessageTransaction`

Przykład encji:

```php
<?php

declare(strict_types=1);

namespace App\Entity\Notifier;

use App\Repository\Notifier\MessageRepository;
use Doctrine\ORM\Mapping as ORM;
use XOne\Bundle\NotifierBundle\Entity\Message as BaseMessage;

#[ORM\Entity(repositoryClass: MessageRepository::class)]
#[ORM\Table(name: 'notifier_message')]
class Message extends BaseMessage
{
}
```

Po ich utworzeniu należy dodać je do konfiguracji:

```yaml
# config/packages/x_one_notifier.yaml
x_one_notifier:
  entities:
    message: App\Entity\Notifier\Message
    message_attachment: App\Entity\Notifier\MessageAttachment
    message_transaction: App\Entity\Notifier\MessageTransaction
    message_context: App\Entity\Notifier\MessageContext
    message_template: App\Entity\Notifier\MessageTemplate
```

## Zapisywanie debugu wysyłki e-mail

Paczka umożliwia zapisywanie debugu wysyłek e-mail (pole `debug` encji `MessageTransaction`).
Przykładowy debug:

```
< 220 mailhog.example ESMTP MailHog
> EHLO [127.0.0.1]
< 250-Hello [127.0.0.1]
< 250-PIPELINING
< 250 AUTH PLAIN
> MAIL FROM:<xone@gmail.com>
< 250 Sender xone@gmail.com ok
> DATA
< 354 End data with <CR><LF>.<CR><LF>
> .
< 250 Ok: queued as jrlA0FgPB62CpaAXv8LzVy4611Jc_KTrY1LJMoojn_g=@mailhog.example
```

Domyślnie funkcjonalność ta jest **włączona**. Aby ją wyłączyć, należy ustawić parametr `save_mailer_debug` na `false`:

```yaml
# config/packages/x_one_notifier.yaml
x_one_notifier:
  save_mailer_debug: false
```

# Instalacja

W pierwszej kolejności dodaj repozytorium do `composer.json`:

```json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/majchw/notifier-bundle.git"
        }
    ]
}
```

Następnie zainstaluj bundle:

```shell
composer require x-one/notifier-bundle
```

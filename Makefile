pre-commit:
	vendor/bin/php-cs-fixer fix
	vendor/bin/phpstan analyse
	vendor/bin/simple-phpunit

<?php

use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use XOne\Bundle\NotifierBundle\Model\MessageAttachmentInterface;
use XOne\Bundle\NotifierBundle\Model\MessageContextInterface;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Model\MessageTemplateInterface;
use XOne\Bundle\NotifierBundle\Model\MessageTransactionInterface;

return static function (DefinitionConfigurator $definition) {
    $definition->rootNode()
        ->children()
            ->arrayNode('entities')
                ->isRequired()
                ->children()
                    ->scalarNode('message')
                        ->isRequired()
                        ->cannotBeEmpty()
                        ->validate()
                            ->ifTrue(fn ($value) => !is_a($value, MessageInterface::class, true))
                            ->thenInvalid(sprintf('Class %%s does not implement the %s.', MessageInterface::class))
                        ->end()
                    ->end()
                    ->scalarNode('message_attachment')
                        ->isRequired()
                        ->cannotBeEmpty()
                        ->validate()
                            ->ifTrue(fn ($value) => !is_a($value, MessageAttachmentInterface::class, true))
                            ->thenInvalid(sprintf('Class %%s does not implement the %s.', MessageAttachmentInterface::class))
                        ->end()
                    ->end()
                    ->scalarNode('message_transaction')
                        ->isRequired()
                        ->cannotBeEmpty()
                        ->validate()
                            ->ifTrue(fn ($value) => !is_a($value, MessageTransactionInterface::class, true))
                            ->thenInvalid(sprintf('Class %%s does not implement the %s.', MessageTransactionInterface::class))
                        ->end()
                    ->end()
                    ->scalarNode('message_context')
                        ->isRequired()
                        ->cannotBeEmpty()
                        ->validate()
                            ->ifTrue(fn ($value) => !is_a($value, MessageContextInterface::class, true))
                            ->thenInvalid(sprintf('Class %%s does not implement the %s.', MessageContextInterface::class))
                        ->end()
                    ->end()
                    ->scalarNode('message_template')
                        ->isRequired()
                        ->cannotBeEmpty()
                        ->validate()
                            ->ifTrue(fn ($value) => !is_a($value, MessageTemplateInterface::class, true))
                            ->thenInvalid(sprintf('Class %%s does not implement the %s.', MessageTemplateInterface::class))
                        ->end()
                    ->end()
            ->end()
        ->end()
        ->booleanNode('save_mailer_debug')->defaultTrue()->end()
    ;
};

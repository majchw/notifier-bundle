<?php

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\Notifier\NotifierInterface;
use XOne\Bundle\NotifierBundle\EventSubscriber\FailedMessageSubscriber;
use XOne\Bundle\NotifierBundle\EventSubscriber\SentMessageSubscriber;
use XOne\Bundle\NotifierBundle\Factory\MessageAttachmentFactory;
use XOne\Bundle\NotifierBundle\Factory\MessageAttachmentFactoryInterface;
use XOne\Bundle\NotifierBundle\Factory\MessageContextFactory;
use XOne\Bundle\NotifierBundle\Factory\MessageContextFactoryInterface;
use XOne\Bundle\NotifierBundle\Factory\MessageFactory;
use XOne\Bundle\NotifierBundle\Factory\MessageFactoryInterface;
use XOne\Bundle\NotifierBundle\Factory\MessageTemplateFactory;
use XOne\Bundle\NotifierBundle\Factory\MessageTemplateFactoryInterface;
use XOne\Bundle\NotifierBundle\Factory\MessageTransactionFactory;
use XOne\Bundle\NotifierBundle\Factory\MessageTransactionFactoryInterface;
use XOne\Bundle\NotifierBundle\Repository\MessageAttachmentRepository;
use XOne\Bundle\NotifierBundle\Repository\MessageAttachmentRepositoryInterface;
use XOne\Bundle\NotifierBundle\Repository\MessageContextRepository;
use XOne\Bundle\NotifierBundle\Repository\MessageContextRepositoryInterface;
use XOne\Bundle\NotifierBundle\Repository\MessageRepository;
use XOne\Bundle\NotifierBundle\Repository\MessageRepositoryInterface;
use XOne\Bundle\NotifierBundle\Repository\MessageTemplateRepository;
use XOne\Bundle\NotifierBundle\Repository\MessageTemplateRepositoryInterface;
use XOne\Bundle\NotifierBundle\Repository\MessageTransactionRepository;
use XOne\Bundle\NotifierBundle\Repository\MessageTransactionRepositoryInterface;
use XOne\Bundle\NotifierBundle\Sender\MessageSenderInterface;
use XOne\Bundle\NotifierBundle\Sender\NotifierMessageSender;
use XOne\Bundle\NotifierBundle\Templating\Renderer\StrReplaceTemplateRenderer;
use XOne\Bundle\NotifierBundle\Templating\Renderer\TemplateRendererInterface;
use XOne\Bundle\NotifierBundle\Templating\Variables\ChainVariableValueProvider;
use XOne\Bundle\NotifierBundle\Templating\Variables\PropertyAccessorVariableValueProvider;
use XOne\Bundle\NotifierBundle\Templating\Variables\VariableValueCollectionProvider;
use XOne\Bundle\NotifierBundle\Templating\Variables\VariableValueCollectionProviderInterface;
use XOne\Bundle\NotifierBundle\Templating\Variables\VariableValueProviderInterface;

use function Symfony\Component\DependencyInjection\Loader\Configurator\param;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;
use function Symfony\Component\DependencyInjection\Loader\Configurator\tagged_iterator;

return static function (ContainerConfigurator $configurator) {
    $services = $configurator->services();

    $services
        ->set('x_one_notifier.sender.notifier', NotifierMessageSender::class)
        ->args([
            service(NotifierInterface::class),
            service(MessageRepositoryInterface::class),
        ])
        ->alias(MessageSenderInterface::class, 'x_one_notifier.sender.notifier')
    ;

    // Templating

    $services
        ->set('x_one_notifier.templating.renderer.str_replace', StrReplaceTemplateRenderer::class)
        ->alias(TemplateRendererInterface::class, 'x_one_notifier.templating.renderer.str_replace')
    ;

    $services
        ->set('x_one_notifier.templating.variable_value_provider.chain', ChainVariableValueProvider::class)
        ->args([tagged_iterator('x_one_notifier.templating.variable_value_provider')])
        ->alias(VariableValueProviderInterface::class, 'x_one_notifier.templating.variable_value_provider.chain')
    ;

    $services
        ->set('x_one_notifier.templating.variable_value_provider.property_accessor', PropertyAccessorVariableValueProvider::class)
        ->tag('x_one_notifier.templating.variable_value_provider')
    ;

    $services
        ->set('x_one_notifier.templating.variable_value_collection_provider', VariableValueCollectionProvider::class)
        ->args([service('x_one_notifier.templating.variable_value_provider.chain')])
        ->alias(VariableValueCollectionProviderInterface::class, 'x_one_notifier.templating.variable_value_collection_provider')
    ;

    // Event subscribers

    $services
        ->set('x_one_notifier.event_subscriber.sent_message', SentMessageSubscriber::class)
        ->args([
            service(MessageRepositoryInterface::class),
            service(MessageTransactionRepositoryInterface::class),
            service('x_one_notifier.factory.message_transaction'),
        ])
        ->tag('kernel.event_subscriber')
    ;

    $services
        ->set('x_one_notifier.event_subscriber.failed_message', FailedMessageSubscriber::class)
        ->args([
            service(MessageRepositoryInterface::class),
            service(MessageTransactionRepositoryInterface::class),
            service('x_one_notifier.factory.message_transaction'),
        ])
        ->tag('kernel.event_subscriber')
    ;

    // Factories

    $services
        ->set('x_one_notifier.factory.message_attachment', MessageAttachmentFactory::class)
        ->args([param('x_one_notifier.entity.message_attachment.class')])
        ->alias(MessageAttachmentFactoryInterface::class, 'x_one_notifier.factory.message_attachment')
    ;

    $services
        ->set('x_one_notifier.factory.message_context', MessageContextFactory::class)
        ->args([param('x_one_notifier.entity.message_context.class')])
        ->alias(MessageContextFactoryInterface::class, 'x_one_notifier.factory.message_context')
    ;

    $services
        ->set('x_one_notifier.factory.message', MessageFactory::class)
        ->args([
            service(TemplateRendererInterface::class),
            service(VariableValueCollectionProviderInterface::class),
            param('x_one_notifier.entity.message.class'),
        ])
        ->alias(MessageFactoryInterface::class, 'x_one_notifier.factory.message')
    ;

    $services
        ->set('x_one_notifier.factory.message_template', MessageTemplateFactory::class)
        ->args([param('x_one_notifier.entity.message_template.class')])
        ->alias(MessageTemplateFactoryInterface::class, 'x_one_notifier.factory.message_template')
    ;

    $services
        ->set('x_one_notifier.factory.message_transaction', MessageTransactionFactory::class)
        ->args([param('x_one_notifier.entity.message_transaction.class')])
        ->alias(MessageTransactionFactoryInterface::class, 'x_one_notifier.factory.message_transaction')
    ;

    // Repositories

    $services
        ->set(MessageAttachmentRepository::class)
        ->tag('doctrine.repository_service')
        ->args([service('doctrine'), param('x_one_notifier.entity.message_context.class')])
        ->alias(MessageAttachmentRepositoryInterface::class, MessageAttachmentRepository::class)
    ;

    $services
        ->set(MessageContextRepository::class)
        ->tag('doctrine.repository_service')
        ->args([service('doctrine'), param('x_one_notifier.entity.message_context.class')])
        ->alias(MessageContextRepositoryInterface::class, MessageContextRepository::class)
    ;

    $services
        ->set(MessageRepository::class)
        ->tag('doctrine.repository_service')
        ->args([service('doctrine'), param('x_one_notifier.entity.message.class')])
        ->alias(MessageRepositoryInterface::class, MessageRepository::class)
    ;

    $services
        ->set(MessageTemplateRepository::class)
        ->tag('doctrine.repository_service')
        ->args([service('doctrine'), param('x_one_notifier.entity.message_template.class')])
        ->alias(MessageTemplateRepositoryInterface::class, MessageTemplateRepository::class)
    ;

    $services
        ->set(MessageTransactionRepository::class)
        ->tag('doctrine.repository_service')
        ->args([service('doctrine'), param('x_one_notifier.entity.message_transaction.class')])
        ->alias(MessageTransactionRepositoryInterface::class, MessageTransactionRepository::class)
    ;
};

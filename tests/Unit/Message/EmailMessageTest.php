<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Message;

use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mime\Address as MimeAddress;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\Recipient;
use XOne\Bundle\NotifierBundle\Entity\MessageAttachment;
use XOne\Bundle\NotifierBundle\Entity\ValueObject\Address;
use XOne\Bundle\NotifierBundle\Mailer\PersistentMessageEmail;
use XOne\Bundle\NotifierBundle\Mailer\PersistentMessageEnvelope;
use XOne\Bundle\NotifierBundle\Message\EmailMessage;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Notification\PersistentMessageNotification;
use XOne\Bundle\NotifierBundle\Recipient\NullRecipient;

class EmailMessageTest extends TestCase
{
    public function testFromNotificationReturnsValidInstance()
    {
        $emailMessage = EmailMessage::fromNotification(
            notification: new Notification(),
            recipient: new Recipient(email: 'foo@example.com'),
        );

        $this->assertInstanceOf(EmailMessage::class, $emailMessage);
    }

    public function testFromNotificationSetsGivenNotification()
    {
        $notification = new Notification();

        $emailMessage = EmailMessage::fromNotification(
            notification: $notification,
            recipient: new NullRecipient(),
        );

        $this->assertEquals($notification, $emailMessage->getNotification());
    }

    public function testFromNotificationCreatesPersistentMessageNotificationEmail()
    {
        $notification = new Notification();
        $notification->subject('Foo');
        $notification->content('Bar');

        $emailMessage = EmailMessage::fromNotification(
            notification: $notification,
            recipient: new NullRecipient(),
        );

        $this->assertInstanceOf(PersistentMessageEmail::class, $emailMessage->getMessage());
        $this->assertEquals('Foo', $emailMessage->getMessage()->getSubject());
        $this->assertEquals('Foo', $emailMessage->getMessage()->getSubject());
        $this->assertEquals('Bar', $emailMessage->getMessage()->getContext()['content']);
        $this->assertEquals('@XOneNotifier/Email/body.html.twig', $emailMessage->getMessage()->getHtmlTemplate());
    }

    public function testFromNotificationUpdatesFromPersistentMessageNotification()
    {
        $message = $this->getPersistentMessageMock();

        $notification = new PersistentMessageNotification($message);

        $emailMessage = EmailMessage::fromNotification(
            notification: $notification,
            recipient: new Recipient(email: 'foo@example.com'),
        );

        $this->assertUpdateFromPersistentMessageCalled($message, $emailMessage);
    }

    public function testUpdatingFromPersistentMessage()
    {
        $message = $this->getPersistentMessageMock();

        $emailMessage = new EmailMessage(new PersistentMessageEmail());
        $emailMessage->updateFromPersistentMessage($message);

        $this->assertUpdateFromPersistentMessageCalled($message, $emailMessage);
    }

    public function testUpdatingFromPersistentMessageWithEnvelope()
    {
        $message = $this->getPersistentMessageMock();

        // This should be mocked, but MIME address class is declared "final", and this somehow results in incompatibility
        $envelope = new Envelope(
            sender: new MimeAddress('foo@example.com'),
            recipients: [
                new MimeAddress('bar@example.com'),
            ],
        );

        $emailMessage = new EmailMessage(new PersistentMessageEmail(), $envelope);
        $emailMessage->updateFromPersistentMessage($message);

        $this->assertEquals(new PersistentMessageEnvelope($message->getId(), $envelope), $emailMessage->getEnvelope());
    }

    public function testAttachingAttachmentsFromPersistentMessage()
    {
        $message = $this->getPersistentMessageMock();
        $message->method('getMessageAttachments')->willReturn(new ArrayCollection([
            (new MessageAttachment())
                ->setFilename('foo'),
            (new MessageAttachment())
                ->setFilename('bar')
                ->setName('baz')
                ->setContentType('image/png'),
        ]));

        $emailMessage = new EmailMessage(new PersistentMessageEmail());
        $emailMessage->updateFromPersistentMessage($message);

        $mimeAttachments = $emailMessage->getMessage()->getAttachments();

        $this->assertCount(2, $mimeAttachments);
        $this->assertEquals('foo', $mimeAttachments[0]->getFilename());
        $this->assertEquals('baz', $mimeAttachments[1]->getFilename());
        $this->assertEquals('image/png', $mimeAttachments[1]->getContentType());
    }

    private function getPersistentMessageMock(): MockObject|MessageInterface
    {
        $message = $this->createMock(MessageInterface::class);
        $message->method('getId')->willReturn(1);
        $message->method('getFrom')->willReturn([new Address('from@example.com', 'From')]);
        $message->method('getCc')->willReturn([new Address('cc@example.com', 'Cc')]);
        $message->method('getBcc')->willReturn([new Address('bcc@example.com', 'Bcc')]);
        $message->method('getReplyTo')->willReturn([new Address('replyto@example.com', 'Reply-To')]);
        $message->method('getSender')->willReturn(new Address('sender@example.com', 'Sender'));
        $message->method('getReturnPath')->willReturn(new Address('returnpath@example.com', 'Return-Path'));
        $message->method('getPriority')->willReturn(5);
        $message->method('getTransport')->willReturn('foo');

        return $message;
    }

    /**
     * @see getPersistentMessageMock
     */
    private function assertUpdateFromPersistentMessageCalled(MessageInterface $message, EmailMessage $emailMessage): void
    {
        $this->assertEquals($message->getId(), $emailMessage->getPersistentMessageId());
        $this->assertEquals([new MimeAddress('from@example.com', 'From')], $emailMessage->getMessage()->getFrom());
        $this->assertEquals([new MimeAddress('cc@example.com', 'Cc')], $emailMessage->getMessage()->getCc());
        $this->assertEquals([new MimeAddress('bcc@example.com', 'Bcc')], $emailMessage->getMessage()->getBcc());
        $this->assertEquals([new MimeAddress('replyto@example.com', 'Reply-To')], $emailMessage->getMessage()->getReplyTo());
        $this->assertEquals(new MimeAddress('sender@example.com', 'Sender'), $emailMessage->getMessage()->getSender());
        $this->assertEquals(new MimeAddress('returnpath@example.com', 'Return-Path'), $emailMessage->getMessage()->getReturnPath());
        $this->assertEquals(5, $emailMessage->getMessage()->getPriority());
        $this->assertEquals('foo', $emailMessage->getTransport());

        $expectedEnvelope = new PersistentMessageEnvelope($message->getId(), null);

        $expectedEnvelope->setRecipients(array_merge(
            $emailMessage->getMessage()->getTo(),
            $emailMessage->getMessage()->getCc(),
            $emailMessage->getMessage()->getBcc(),
        ));

        $this->assertEquals($expectedEnvelope, $emailMessage->getEnvelope());
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Message;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Notifier\Notification\Notification;
use XOne\Bundle\NotifierBundle\Message\ChatMessage;
use XOne\Bundle\NotifierBundle\Message\MessageOptions;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Notification\PersistentMessageNotification;

class ChatMessageTest extends TestCase
{
    public function testFromNotificationReturnsValidInstance()
    {
        $this->assertInstanceOf(ChatMessage::class, ChatMessage::fromNotification(new Notification()));
    }

    public function testFromNotificationSetsGivenNotification()
    {
        $notification = new Notification();

        $chatMessage = ChatMessage::fromNotification($notification);

        $this->assertEquals($notification, $chatMessage->getNotification());
    }

    public function testFromNotificationSetsMessageSubject()
    {
        $notification = new Notification();
        $notification->subject('Foo Bar');

        $chatMessage = ChatMessage::fromNotification($notification);

        $this->assertEquals('Foo Bar', $chatMessage->getSubject());
    }

    public function testFromNotificationUpdatesFromPersistentMessageNotification()
    {
        $message = $this->getPersistentMessageMock();

        $notification = new PersistentMessageNotification($message);

        $chatMessage = ChatMessage::fromNotification($notification);

        $this->assertUpdateFromPersistentMessageCalled($message, $chatMessage);
    }

    public function testUpdatingFromPersistentMessage()
    {
        $message = $this->getPersistentMessageMock();

        $chatMessage = new ChatMessage('Foo');
        $chatMessage->updateFromPersistentMessage($message);

        $this->assertUpdateFromPersistentMessageCalled($message, $chatMessage);
    }

    private function getPersistentMessageMock(): MessageInterface
    {
        $messageOptions = new MessageOptions(['foo' => 'bar'], 'baz');

        $message = $this->createMock(MessageInterface::class);
        $message->method('getId')->willReturn(1);
        $message->method('getTransport')->willReturn('foo');
        $message->method('getOptions')->willReturn($messageOptions);

        return $message;
    }

    private function assertUpdateFromPersistentMessageCalled(MessageInterface $message, ChatMessage $chatMessage): void
    {
        $this->assertEquals($message->getId(), $chatMessage->getPersistentMessageId());
        $this->assertEquals($message->getTransport(), $chatMessage->getTransport());
        $this->assertEquals($message->getOptions(), $chatMessage->getOptions());
    }
}

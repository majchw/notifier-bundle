<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Message;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\Recipient;
use XOne\Bundle\NotifierBundle\Entity\ValueObject\Address;
use XOne\Bundle\NotifierBundle\Exception\LogicException;
use XOne\Bundle\NotifierBundle\Message\MessageOptions;
use XOne\Bundle\NotifierBundle\Message\SmsMessage;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Notification\PersistentMessageNotification;

class SmsMessageTest extends TestCase
{
    public function testFromNotificationReturnsValidInstance()
    {
        $smsMessage = SmsMessage::fromNotification(
            notification: new Notification(),
            recipient: new Recipient(phone: '123456789'),
        );

        $this->assertInstanceOf(SmsMessage::class, $smsMessage);
    }

    public function testFromNotificationSetsGivenNotification()
    {
        $notification = new Notification();

        $smsMessage = SmsMessage::fromNotification(
            notification: $notification,
            recipient: new Recipient(phone: '123456789'),
        );

        $this->assertEquals($notification, $smsMessage->getNotification());
    }

    public function testFromNotificationSetsMessagePhoneAndSubject()
    {
        $notification = new Notification();
        $notification->subject('Foo');

        $smsMessage = SmsMessage::fromNotification(
            notification: $notification,
            recipient: new Recipient(phone: '123456789'),
        );

        $this->assertEquals('Foo', $smsMessage->getSubject());
        $this->assertEquals('123456789', $smsMessage->getPhone());
    }

    public function testFromNotificationUpdatesFromPersistentMessageNotification()
    {
        $message = $this->getPersistentMessageMock();

        $notification = new PersistentMessageNotification($message);

        $smsMessage = SmsMessage::fromNotification(
            notification: $notification,
            recipient: new Recipient(phone: '123456789'),
        );

        $this->assertUpdateFromPersistentMessageCalled($message, $smsMessage);
    }

    public function testUpdatingFromPersistentMessage()
    {
        $message = $this->getPersistentMessageMock();

        $smsMessage = new SmsMessage('Foo', 'Bar');
        $smsMessage->updateFromPersistentMessage($message);

        $this->assertUpdateFromPersistentMessageCalled($message, $smsMessage);
    }

    public function testUpdatingFromPersistentMessageWithSingleFrom()
    {
        $message = $this->getPersistentMessageMock();
        $message->method('getFrom')->willReturn([new Address('111111111')]);

        $smsMessage = new SmsMessage('123456789', 'Foo');
        $smsMessage->updateFromPersistentMessage($message);

        $this->assertUpdateFromPersistentMessageCalled($message, $smsMessage);
    }

    public function testUpdatingFromPersistentMessageWithMoreThanOneFrom()
    {
        $message = $this->getPersistentMessageMock();
        $message->method('getFrom')->willReturn([new Address('111111111'), new Address('222222222')]);

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('The SMS message can only have one sender.');

        $smsMessage = new SmsMessage('123456789', 'Foo');
        $smsMessage->updateFromPersistentMessage($message);
    }

    private function getPersistentMessageMock(): MockObject|MessageInterface
    {
        $messageOptions = new MessageOptions(['foo' => 'bar'], 'baz');

        $message = $this->createMock(MessageInterface::class);
        $message->method('getId')->willReturn(1);
        $message->method('getTransport')->willReturn('foo');
        $message->method('getOptions')->willReturn($messageOptions);

        return $message;
    }

    private function assertUpdateFromPersistentMessageCalled(MessageInterface $message, SmsMessage $smsMessage): void
    {
        $this->assertEquals($message->getId(), $smsMessage->getPersistentMessageId());
        $this->assertEquals($message->getTransport(), $smsMessage->getTransport());
        $this->assertEquals($message->getOptions(), $smsMessage->getOptions());
        $this->assertEquals((string) current($message->getFrom()), $smsMessage->getFrom());
    }
}

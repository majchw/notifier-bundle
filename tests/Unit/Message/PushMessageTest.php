<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Message;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Notifier\Notification\Notification;
use XOne\Bundle\NotifierBundle\Message\MessageOptions;
use XOne\Bundle\NotifierBundle\Message\PushMessage;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Notification\PersistentMessageNotification;

class PushMessageTest extends TestCase
{
    public function testFromNotificationReturnsValidInstance()
    {
        $this->assertInstanceOf(PushMessage::class, PushMessage::fromNotification(new Notification()));
    }

    public function testFromNotificationSetsGivenNotification()
    {
        $notification = new Notification();

        $pushMessage = PushMessage::fromNotification($notification);

        $this->assertEquals($notification, $pushMessage->getNotification());
    }

    public function testFromNotificationSetsMessageSubjectAndContent()
    {
        $notification = new Notification();
        $notification->subject('Foo');
        $notification->content('Bar');

        $pushMessage = PushMessage::fromNotification($notification);

        $this->assertEquals('Foo', $pushMessage->getSubject());
        $this->assertEquals('Bar', $pushMessage->getContent());
    }

    public function testFromNotificationUpdatesFromPersistentMessageNotification()
    {
        $message = $this->getPersistentMessageMock();

        $notification = new PersistentMessageNotification($message);

        $pushMessage = PushMessage::fromNotification($notification);

        $this->assertUpdateFromPersistentMessageCalled($message, $pushMessage);
    }

    public function testUpdatingFromPersistentMessage()
    {
        $message = $this->getPersistentMessageMock();

        $pushMessage = new PushMessage('Foo', 'Bar');
        $pushMessage->updateFromPersistentMessage($message);

        $this->assertUpdateFromPersistentMessageCalled($message, $pushMessage);
    }

    private function getPersistentMessageMock(): MessageInterface
    {
        $messageOptions = new MessageOptions(['foo' => 'bar'], 'baz');

        $message = $this->createMock(MessageInterface::class);
        $message->method('getId')->willReturn(1);
        $message->method('getTransport')->willReturn('foo');
        $message->method('getOptions')->willReturn($messageOptions);

        return $message;
    }

    private function assertUpdateFromPersistentMessageCalled(MessageInterface $message, PushMessage $pushMessage): void
    {
        $this->assertEquals($message->getId(), $pushMessage->getPersistentMessageId());
        $this->assertEquals($message->getTransport(), $pushMessage->getTransport());
        $this->assertEquals($message->getOptions(), $pushMessage->getOptions());
    }
}

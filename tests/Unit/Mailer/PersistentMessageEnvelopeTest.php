<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Mailer;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mime\Address as MimeAddress;
use XOne\Bundle\NotifierBundle\Mailer\PersistentMessageEnvelope;

class PersistentMessageEnvelopeTest extends TestCase
{
    public function testRetrievingOriginalEnvelope()
    {
        $originalEnvelope = $this->getMailerEnvelope();

        $envelope = new PersistentMessageEnvelope(1, $originalEnvelope);

        $this->assertEquals($originalEnvelope, $envelope->getOriginalEnvelope());
    }

    public function testPassingPersistentMessageId()
    {
        $envelope = new PersistentMessageEnvelope(1, $this->getMailerEnvelope());

        $this->assertEquals(1, $envelope->getPersistentMessageId());
    }

    private function getMailerEnvelope(): Envelope
    {
        return new Envelope(
            sender: new MimeAddress('foo@example.com'),
            recipients: [
                new MimeAddress('bar@example.com'),
            ],
        );
    }
}

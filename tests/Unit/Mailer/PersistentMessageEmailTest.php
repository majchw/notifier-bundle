<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Mailer;

use PHPUnit\Framework\TestCase;
use XOne\Bundle\NotifierBundle\Mailer\PersistentMessageEmail;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;

class PersistentMessageEmailTest extends TestCase
{
    public function testUpdatingFromPersistentMessage()
    {
        $message = $this->createMock(MessageInterface::class);
        $message->method('getId')->willReturn(1);

        $email = new PersistentMessageEmail();
        $email->updateFromPersistentMessage($message);

        $this->assertEquals(1, $email->getPersistentMessageId());
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Notification;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Notifier\Recipient\NoRecipient;
use Symfony\Component\Notifier\Recipient\Recipient;
use XOne\Bundle\NotifierBundle\Entity\Message;
use XOne\Bundle\NotifierBundle\Entity\ValueObject\Address;
use XOne\Bundle\NotifierBundle\Message\ChatMessage;
use XOne\Bundle\NotifierBundle\Message\EmailMessage;
use XOne\Bundle\NotifierBundle\Message\PushMessage;
use XOne\Bundle\NotifierBundle\Message\SmsMessage;
use XOne\Bundle\NotifierBundle\Notification\PersistentMessageNotification;
use XOne\Bundle\NotifierBundle\Recipient\NullRecipient;

class PersistentMessageNotificationTest extends TestCase
{
    public function testConstructorSetsDataFromPersistentMessage()
    {
        $message = $this->createMessage('Foo', 'Bar', 'email');

        $notification = $this->createNotification($message);

        $this->assertEquals('Foo', $notification->getSubject());
        $this->assertEquals('Bar', $notification->getContent());
        $this->assertEquals(['email'], $notification->getChannels(new NoRecipient()));
    }

    public function testDataSetInConstructorCanBeOverwritten()
    {
        $message = $this->createMessage('Foo', 'Bar', 'email');

        $notification = $this->createNotification($message)
            ->subject('Lorem')
            ->content('Ipsum')
            ->channels(['sms'])
        ;

        $this->assertEquals('Lorem', $notification->getSubject());
        $this->assertEquals('Ipsum', $notification->getContent());
        $this->assertEquals(['sms'], $notification->getChannels(new NoRecipient()));
    }

    public function testAsEmailMessageReturnsValidEmailMessage()
    {
        $message = $this->createMessage()->addTo(new Address('test@example.com'));

        $emailMessage = $this->createNotification($message)->asEmailMessage(new NullRecipient());

        $this->assertInstanceOf(EmailMessage::class, $emailMessage);
    }

    public function testAsEmailMessageWithExplicitTransport()
    {
        $message = $this->createMessage()
            ->addTo(new Address('test@example.com'))
            ->setTransport('sendmail');

        $emailMessage = $this->createNotification($message)->asEmailMessage(new NullRecipient());

        $this->assertEquals('sendmail', $emailMessage->getTransport());
    }

    public function testAsSmsMessageReturnsValidSmsMessage()
    {
        $smsMessage = $this->createNotification()->asSmsMessage(new Recipient(phone: '123456789'));

        $this->assertInstanceOf(SmsMessage::class, $smsMessage);
    }

    public function testAsSmsMessageWithExplicitTransport()
    {
        $message = $this->createMessage()->setTransport('smsapi');

        $smsMessage = $this->createNotification($message)->asSmsMessage(new Recipient(phone: '123456789'));

        $this->assertEquals('smsapi', $smsMessage->getTransport());
    }

    public function testAsChatMessageReturnsValidChatMessage()
    {
        $chatMessage = $this->createNotification()->asChatMessage(new NoRecipient());

        $this->assertInstanceOf(ChatMessage::class, $chatMessage);
    }

    public function testAsChatMessageWithExplicitTransport()
    {
        $message = $this->createMessage()->setTransport('rocketchat');

        $chatMessage = $this->createNotification($message)->asChatMessage(new NoRecipient());

        $this->assertEquals('rocketchat', $chatMessage->getTransport());
    }

    public function testAsPushMessageReturnsValidPushMessage()
    {
        $pushMessage = $this->createNotification()->asPushMessage(new NoRecipient());

        $this->assertInstanceOf(PushMessage::class, $pushMessage);
    }

    public function testAsPushMessageWithExplicitTransport()
    {
        $message = $this->createMessage()->setTransport('pushover');

        $pushMessage = $this->createNotification($message)->asPushMessage(new NoRecipient());

        $this->assertEquals('pushover', $pushMessage->getTransport());
    }

    private function createMessage(string $subject = null, string $content = null, string $channel = null): Message
    {
        return (new Message())
            ->setSubject($subject)
            ->setContent($content)
            ->setChannel($channel)
        ;
    }

    private function createNotification(Message $message = null): PersistentMessageNotification
    {
        return new PersistentMessageNotification($message ?? $this->createMessage());
    }
}

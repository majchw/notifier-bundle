<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Templating\Variables;

use PHPUnit\Framework\TestCase;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use XOne\Bundle\NotifierBundle\Exception\VariableValueProviderException;
use XOne\Bundle\NotifierBundle\Templating\Variables\PropertyAccessorVariableValueProvider;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\PropertyAccess\NullPropertyAccessor;

class PropertyAccessorVariableValueProviderTest extends TestCase
{
    public function testRetrievingValueFromArray()
    {
        $this->assertEquals('bar', $this->createProvider()->getValue('foo', ['foo' => 'bar']));
    }

    public function testRetrievingValueFromObject()
    {
        $this->assertEquals('bar', $this->createProvider()->getValue('foo', (object) ['foo' => 'bar']));
    }

    public function testRetrievingValueFromNonArrayNorObjectThrowsException()
    {
        $this->expectException(VariableValueProviderException::class);

        $this->createProvider()->getValue('foo', 'bar');
    }

    public function testRetrievingInvalidIndexThrowsException()
    {
        $this->expectException(VariableValueProviderException::class);

        $this->createProvider()->getValue('foo', []);
    }

    public function testRetrievingInvalidPropertyPathThrowsException()
    {
        $this->expectException(VariableValueProviderException::class);

        $this->createProvider()->getValue('foo', (object) []);
    }

    public function testProviderUsingCustomPropertyAccessor()
    {
        $this->assertEquals(null, $this->createProvider(new NullPropertyAccessor())->getValue('foo', ['foo' => 'bar']));
    }

    private function createProvider(PropertyAccessorInterface $propertyAccessor = null): PropertyAccessorVariableValueProvider
    {
        return new PropertyAccessorVariableValueProvider($propertyAccessor);
    }
}

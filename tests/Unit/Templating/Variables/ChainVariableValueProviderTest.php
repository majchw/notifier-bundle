<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Templating\Variables;

use PHPUnit\Framework\TestCase;
use XOne\Bundle\NotifierBundle\Exception\VariableValueProviderException;
use XOne\Bundle\NotifierBundle\Templating\Variables\ChainVariableValueProvider;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Templating\Variables\ExceptionThrowingVariableValueProvider;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Templating\Variables\SuccessfulVariableValueProvider;

class ChainVariableValueProviderTest extends TestCase
{
    public function testGettingValueUsesFirstSuccessfulProvider()
    {
        $provider = $this->createChainProvider([
            $this->createSuccessfulThrowingProvider('foo'),
            $this->createSuccessfulThrowingProvider('bar'),
        ]);

        $this->assertEquals('foo', $provider->getValue('baz'));
    }

    public function testReturningNullValueDoesNotSkipProvider()
    {
        $provider = $this->createChainProvider([
            $this->createSuccessfulThrowingProvider(null),
            $this->createSuccessfulThrowingProvider('foo'),
        ]);

        $this->assertEquals(null, $provider->getValue('bar'));
    }

    public function testVariableValueProviderExceptionSkipsProvider()
    {
        $provider = $this->createChainProvider([
            $this->createExceptionThrowingProvider(new VariableValueProviderException()),
            $this->createSuccessfulThrowingProvider('foo'),
        ]);

        $this->assertEquals('foo', $provider->getValue('bar'));
    }

    public function testExceptionsExceptNonVariableValueProviderExceptionDoesNotSkipProvider()
    {
        $provider = $this->createChainProvider([
            $this->createExceptionThrowingProvider(new \RuntimeException()),
            $this->createSuccessfulThrowingProvider('foo'),
        ]);

        $this->expectException(\RuntimeException::class);

        $provider->getValue('bar');
    }

    private function createChainProvider(iterable $providers = []): ChainVariableValueProvider
    {
        return new ChainVariableValueProvider($providers);
    }

    private function createSuccessfulThrowingProvider(mixed $value): SuccessfulVariableValueProvider
    {
        return new SuccessfulVariableValueProvider($value);
    }

    private function createExceptionThrowingProvider(\Throwable $exception): ExceptionThrowingVariableValueProvider
    {
        return new ExceptionThrowingVariableValueProvider($exception);
    }
}

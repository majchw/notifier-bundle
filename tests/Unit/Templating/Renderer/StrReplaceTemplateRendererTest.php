<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Templating\Renderer;

use PHPUnit\Framework\TestCase;
use XOne\Bundle\NotifierBundle\Templating\Renderer\StrReplaceTemplateRenderer;

class StrReplaceTemplateRendererTest extends TestCase
{
    public static function templateProvider(): array
    {
        return [
            ['Welcome {{ user_name }} to {{ app_name }}'],
            ['Welcome {{user_name }} to {{ app_name}}'],
            ['Welcome {{user_name}} to {{app_name}}'],
        ];
    }

    /**
     * @dataProvider templateProvider
     */
    public function testRendering(string $template)
    {
        $renderer = $this->createRenderer();

        $this->assertEquals('Welcome Elliot to E-Corp', $renderer->render($template, [
            'user_name' => 'Elliot',
            'app_name' => 'E-Corp',
        ]));
    }

    public function testRenderingWithVariablesNotWrappedInHandlebars()
    {
        $renderer = $this->createRenderer();

        $template = 'Welcome user_name to app_name';

        $this->assertEquals($template, $renderer->render($template, [
            'user_name' => 'Axel',
            'app_name' => 'the jungle',
        ]));
    }

    private function createRenderer(): StrReplaceTemplateRenderer
    {
        return new StrReplaceTemplateRenderer();
    }
}

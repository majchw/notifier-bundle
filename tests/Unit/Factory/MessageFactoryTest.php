<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Factory;

use PHPUnit\Framework\TestCase;
use XOne\Bundle\NotifierBundle\Entity\Message;
use XOne\Bundle\NotifierBundle\Entity\MessageTemplate;
use XOne\Bundle\NotifierBundle\Exception\LogicException;
use XOne\Bundle\NotifierBundle\Factory\MessageFactory;
use XOne\Bundle\NotifierBundle\Model\MessageTemplateInterface;
use XOne\Bundle\NotifierBundle\Templating\Renderer\TemplateRendererInterface;
use XOne\Bundle\NotifierBundle\Templating\Variables\VariableValueCollectionProviderInterface;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\Message as CustomMessage;

class MessageFactoryTest extends TestCase
{
    /**
     * @dataProvider
     */
    public static function createMethodProvider(): array
    {
        return [
            ['create'],
            ['createEmail'],
            ['createSms'],
            ['createChat'],
            ['createPush'],
        ];
    }

    /**
     * @dataProvider createMethodProvider
     */
    public function testCreateReturnsValidInstanceWithoutCustomClass(string $method)
    {
        $message = $this->getMessageFactoryWithoutCustomClass()->{$method}();

        $this->assertInstanceOf(Message::class, $message);
    }

    /**
     * @dataProvider createMethodProvider
     */
    public function testCreateReturnsValidInstanceWhenUsingCustomClass(string $method)
    {
        $message = $this->getMessageFactoryWithCustomClass()->{$method}();

        $this->assertInstanceOf(CustomMessage::class, $message);
    }

    /**
     * @dataProvider
     */
    public static function createTemplatedMethodProvider(): array
    {
        return [
            ['createTemplated', null],
            ['createTemplatedEmail', 'email'],
            ['createTemplatedSms', 'sms'],
            ['createTemplatedChat', 'chat'],
            ['createTemplatedPush', 'push'],
        ];
    }

    /**
     * @dataProvider createTemplatedMethodProvider
     */
    public function testCreateTemplatedReturnsValidInstanceWhenNotUsingCustomClass(string $method, ?string $channel)
    {
        $messageTemplate = $this->createMock(MessageTemplateInterface::class);
        $messageTemplate->method('getChannels')->willReturn(array_filter([$channel]));

        $message = $this->getMessageFactoryWithoutCustomClass()->{$method}($messageTemplate);

        $this->assertInstanceOf(Message::class, $message);
    }

    /**
     * @dataProvider createTemplatedMethodProvider
     */
    public function testCreateTemplatedReturnsValidInstanceWhenUsingCustomClass(string $method, ?string $channel)
    {
        $messageTemplate = $this->createMock(MessageTemplateInterface::class);
        $messageTemplate->method('getChannels')->willReturn(array_filter([$channel]));

        $message = $this->getMessageFactoryWithCustomClass()->{$method}($messageTemplate);

        $this->assertInstanceOf(Message::class, $message);
    }

    public function testCreate()
    {
        $message = $this->getMessageFactoryWithoutCustomClass()->create('foo', 'bar', 'baz');

        $this->assertEquals('foo', $message->getSubject());
        $this->assertEquals('bar', $message->getContent());
        $this->assertEquals('baz', $message->getChannel());
    }

    public function testCreateTemplated()
    {
        $messageTemplate = new MessageTemplate();
        $messageTemplate->setChannels(['foo']);

        $message = $this->getMessageFactoryWithoutCustomClass()->createTemplated($messageTemplate);

        // Subject and content is not set, because a template renderer mock is used.
        // Note: the template renderer is unit tested *separately*.
        $this->assertEquals('foo', $message->getChannel());
    }

    public function testCreateWithoutChannelSetsDefaultEmailChannel()
    {
        $message = $this->getMessageFactoryWithoutCustomClass()->create();

        $this->assertEquals('email', $message->getChannel());
    }

    public function testCreateTemplatedWithoutChannelSetsDefaultEmailChannel()
    {
        $messageTemplate = $this->createMock(MessageTemplateInterface::class);

        $message = $this->getMessageFactoryWithoutCustomClass()->createTemplated($messageTemplate);

        $this->assertEquals('email', $message->getChannel());
    }

    public function testCreateTemplatedWithMultipleChannelsWithoutSpecifyingWhichOne()
    {
        $messageTemplate = $this->createMock(MessageTemplateInterface::class);
        $messageTemplate->method('getChannels')->willReturn(['email', 'sms', 'chat', 'push']);

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Template used to create a message has more than one channel. You must specify which channel ("email", "sms", "chat", "push") the message should use.');

        $this->getMessageFactoryWithoutCustomClass()->createTemplated($messageTemplate);
    }

    public function testCreateTemplatedCallsRendererToSetMessageContent()
    {
        $messageTemplate = $this->createMock(MessageTemplateInterface::class);

        $templateRenderer = $this->createMock(TemplateRendererInterface::class);
        $templateRenderer->method('render')->willReturn('Rendered content');

        $messageFactory = new MessageFactory(
            $templateRenderer,
            $this->createMock(VariableValueCollectionProviderInterface::class),
        );

        $message = $messageFactory->createTemplated($messageTemplate);

        $this->assertEquals('Rendered content', $message->getContent());
    }

    public function testCreateTemplatedCallsVariableValueCollectionProviderToGetVariablesToPassToRenderer()
    {
        $messageTemplate = $this->createMock(MessageTemplateInterface::class);
        $messageTemplate->method('getSubject')->willReturn('foo');
        $messageTemplate->method('getContent')->willReturn('bar');

        $variableValueCollectionProvider = $this->createMock(VariableValueCollectionProviderInterface::class);
        $variableValueCollectionProvider->method('getValues')->willReturn(['foo' => 'bar']);

        $templateRenderer = $this->createMock(TemplateRendererInterface::class);

        $templateRenderer
            ->expects($matcher = $this->exactly(2))
            ->method('render')
            ->with($this->callback(
                fn (string $template) => match ($matcher->getInvocationCount()) {
                    1 => 'foo' === $template,
                    2 => 'bar' === $template,
                    default => false,
                }
            ), $this->equalTo(['foo' => 'bar']))
        ;

        $messageFactory = new MessageFactory($templateRenderer, $variableValueCollectionProvider);
        $messageFactory->createTemplated($messageTemplate);
    }

    private function getMessageFactoryWithoutCustomClass(): MessageFactory
    {
        return new MessageFactory(
            $this->createMock(TemplateRendererInterface::class),
            $this->createMock(VariableValueCollectionProviderInterface::class),
        );
    }

    private function getMessageFactoryWithCustomClass(): MessageFactory
    {
        return new MessageFactory(
            $this->createMock(TemplateRendererInterface::class),
            $this->createMock(VariableValueCollectionProviderInterface::class),
            CustomMessage::class,
        );
    }
}

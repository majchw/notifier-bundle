<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Factory;

use PHPUnit\Framework\TestCase;
use XOne\Bundle\NotifierBundle\Entity\Message;
use XOne\Bundle\NotifierBundle\Entity\MessageTransaction;
use XOne\Bundle\NotifierBundle\Factory\MessageTransactionFactory;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\MessageTransaction as CustomMessageTransaction;

class MessageTransactionFactoryTest extends TestCase
{
    public function testCreateReturnsValidInstance()
    {
        $factory = new MessageTransactionFactory();

        $messageTransaction = $factory->create();

        $this->assertInstanceOf(MessageTransaction::class, $messageTransaction);
    }

    public function testCreateReturnsValidInstanceWhenUsingCustomClass()
    {
        $factory = new MessageTransactionFactory(CustomMessageTransaction::class);

        $messageTransaction = $factory->create();

        $this->assertInstanceOf(CustomMessageTransaction::class, $messageTransaction);
    }

    public function testCreateSuccessful()
    {
        $factory = new MessageTransactionFactory();

        $message = new Message();
        $messageTransaction = $factory->createSuccessful($message, 'foo');

        $this->assertTrue($messageTransaction->isSuccessful());
        $this->assertEquals($message, $messageTransaction->getMessage());
        $this->assertEquals('foo', $messageTransaction->getTransportMessageId());
    }

    public function testCreateSuccessfulWithoutArguments()
    {
        $factory = new MessageTransactionFactory();

        $messageTransaction = $factory->createSuccessful();

        $this->assertTrue($messageTransaction->isSuccessful());
        $this->assertNull($messageTransaction->getMessage());
        $this->assertNull($messageTransaction->getTransportMessageId());
    }

    public function testCreateFailed()
    {
        $factory = new MessageTransactionFactory();

        $message = new Message();
        $exception = new \Exception('Foo bar');
        $messageTransaction = $factory->createFailed($message, $exception);

        $this->assertFalse($messageTransaction->isSuccessful());
        $this->assertEquals($message, $messageTransaction->getMessage());
        $this->assertEquals('Foo bar', $messageTransaction->getException());
    }

    public function testCreateFailedWithoutArguments()
    {
        $factory = new MessageTransactionFactory();

        $messageTransaction = $factory->createFailed();

        $this->assertFalse($messageTransaction->isSuccessful());
        $this->assertNull($messageTransaction->getMessage());
        $this->assertNull($messageTransaction->getTransportMessageId());
    }
}

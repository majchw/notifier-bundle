<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Factory;

use PHPUnit\Framework\TestCase;
use XOne\Bundle\NotifierBundle\Entity\MessageAttachment;
use XOne\Bundle\NotifierBundle\Factory\MessageAttachmentFactory;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\MessageAttachment as CustomMessageAttachment;

class MessageAttachmentFactoryTest extends TestCase
{
    public function testCreateReturnsValidInstance()
    {
        $factory = new MessageAttachmentFactory();

        $message = $factory->create('foo');

        $this->assertInstanceOf(MessageAttachment::class, $message);
    }

    public function testCreateReturnsValidInstanceWhenUsingCustomClass()
    {
        $factory = new MessageAttachmentFactory(CustomMessageAttachment::class);

        $message = $factory->create('foo');

        $this->assertInstanceOf(CustomMessageAttachment::class, $message);
    }

    public function testCreateProperlySetsData()
    {
        $factory = new MessageAttachmentFactory();

        $message = $factory->create('foo', 'bar', 'baz');

        $this->assertEquals('foo', $message->getFilename());
        $this->assertEquals('bar', $message->getName());
        $this->assertEquals('baz', $message->getContentType());
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Factory;

use PHPUnit\Framework\TestCase;
use XOne\Bundle\NotifierBundle\Entity\MessageContext;
use XOne\Bundle\NotifierBundle\Entity\MessageTemplate;
use XOne\Bundle\NotifierBundle\Factory\MessageTemplateFactory;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\MessageTemplate as CustomMessageTemplate;

class MessageTemplateFactoryTest extends TestCase
{
    public function testCreateReturnsValidInstance()
    {
        $factory = new MessageTemplateFactory();

        $message = $factory->create('foo');

        $this->assertInstanceOf(MessageTemplate::class, $message);
    }

    public function testCreateReturnsValidInstanceWhenUsingCustomClass()
    {
        $factory = new MessageTemplateFactory(CustomMessageTemplate::class);

        $message = $factory->create('foo');

        $this->assertInstanceOf(CustomMessageTemplate::class, $message);
    }

    public function testCreateProperlySetsData()
    {
        $factory = new MessageTemplateFactory();

        $message = $factory->create('foo', 'bar', 'baz', ['foo' => 'bar'], ['bar' => 'baz']);

        $this->assertEquals('foo', $message->getName());
        $this->assertEquals('bar', $message->getSubject());
        $this->assertEquals('baz', $message->getContent());
        $this->assertEquals(['foo' => 'bar'], $message->getVariables());
        $this->assertEquals(['bar' => 'baz'], $message->getChannels());
    }

    public function testCreateFromMessageContext()
    {
        $messageContext = new MessageContext();
        $messageContext->setName('foo');
        $messageContext->setVariables(['foo' => 'bar']);
        $messageContext->setChannels(['bar' => 'baz']);

        $factory = new MessageTemplateFactory();

        $message = $factory->createFromMessageContext($messageContext, 'bar');

        $this->assertEquals('foo', $message->getName());
        $this->assertEquals('foo', $message->getSubject());
        $this->assertEquals('bar', $message->getContent());
        $this->assertEquals(['foo' => 'bar'], $message->getVariables());
        $this->assertEquals(['bar' => 'baz'], $message->getChannels());
    }
}

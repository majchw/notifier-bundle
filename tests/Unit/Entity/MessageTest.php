<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Entity;

use PHPUnit\Framework\TestCase;
use XOne\Bundle\NotifierBundle\Entity\Message;
use XOne\Bundle\NotifierBundle\Message\MessageOptions;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Message\CustomMessageOptions;

class MessageTest extends TestCase
{
    public function testSettingRecipientIdChangesOptionsRecipientId()
    {
        $message = new Message();
        $message->setRecipientId('test-123');

        $this->assertEquals('test-123', $message->getOptions()->getRecipientId());
    }

    public function testSettingCustomOptions()
    {
        $message = new Message();
        $message->setOptions(new CustomMessageOptions([
            'recipient-id' => 'test-123',
            'foo' => 'bar',
        ]));

        $expected = new MessageOptions(['recipient-id' => 'test-123', 'foo' => 'bar'], 'test-123');

        $this->assertEquals('test-123', $message->getRecipientId());
        $this->assertEquals($expected, $message->getOptions());
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Entity\ValueObject;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Mime\Address as MimeAddress;
use XOne\Bundle\NotifierBundle\Entity\ValueObject\Address;

class AddressTest extends TestCase
{
    /**
     * @dataProvider
     */
    public static function castingToStringAddressProvider(): array
    {
        return [
            [new Address(), ''],
            [new Address('foo'), 'foo'],
            [new Address('foo', 'bar'), 'bar <foo>'],
            [new Address(null, 'bar'), 'bar'],
        ];
    }

    /**
     * @dataProvider castingToStringAddressProvider
     */
    public function testCastingToString(Address $address, string $expected)
    {
        $this->assertEquals($expected, (string) $address);
    }

    /**
     * @dataProvider
     */
    public static function jsonSerializationAddressProvider(): array
    {
        return [
            [new Address(), []],
            [new Address('foo'), ['address' => 'foo']],
            [new Address('foo', 'bar'), ['address' => 'foo', 'name' => 'bar']],
            [new Address(null, 'bar'), ['name' => 'bar']],
        ];
    }

    /**
     * @dataProvider jsonSerializationAddressProvider
     */
    public function testJsonSerialization(Address $address, array $expected)
    {
        $this->assertEquals(json_encode($expected), json_encode($address));
    }

    /**
     * @dataProvider
     */
    public static function mimeAddressProvider(): array
    {
        return [
            [new MimeAddress('foo@example.com'), new Address('foo@example.com')],
            [new MimeAddress('foo@example.com', ''), new Address('foo@example.com')],
            [new MimeAddress('foo@example.com', 'bar'), new Address('foo@example.com', 'bar')],
        ];
    }

    /**
     * @dataProvider mimeAddressProvider
     */
    public function testCreatingFromMimeAddress(MimeAddress $mimeAddress, Address $expected)
    {
        $this->assertEquals($expected, Address::fromMimeAddress($mimeAddress));
    }

    /**
     * @dataProvider mimeAddressProvider
     */
    public function testConvertingToMimeAddress(MimeAddress $expected, Address $address)
    {
        $this->assertEquals($expected, $address->toMimeAddress());
    }
}

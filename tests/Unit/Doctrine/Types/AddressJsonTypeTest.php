<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Unit\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use PHPUnit\Framework\TestCase;
use XOne\Bundle\NotifierBundle\Doctrine\Types\AddressJsonType;
use XOne\Bundle\NotifierBundle\Entity\ValueObject\Address;

class AddressJsonTypeTest extends TestCase
{
    private AddressJsonType $type;

    /**
     * @dataProvider
     */
    public static function addressProvider()
    {
        return [
            [null, []],
            ['', []],
            [[], []],
            [[['address' => 'foo']], [new Address('foo')]],
            [[['address' => 'foo', 'name' => 'bar']], [new Address('foo', 'bar')]],
            [[['name' => 'bar']], [new Address(null, 'bar')]],
            [[['foo' => 'bar']], [new Address()]],
            [[[]], [new Address()]],
        ];
    }

    protected function setUp(): void
    {
        $this->type = new AddressJsonType();
    }

    /**
     * @dataProvider addressProvider
     */
    public function testConvertingToPHPValue($value, $expected)
    {
        $platform = $this->createMock(AbstractPlatform::class);

        $this->assertEquals($expected, $this->type->convertToPHPValue(json_encode($value), $platform));
    }

    public function testName()
    {
        $expected = 'x_one_notifier_address_json';

        $this->assertEquals($expected, $this->type->getName());
        $this->assertEquals($expected, $this->type::NAME);
    }
}

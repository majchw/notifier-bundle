<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Integration\Sender;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Mime\Address as MimeAddress;
use Symfony\Component\Notifier\Exception\LogicException as NotifierLogicException;
use XOne\Bundle\NotifierBundle\Entity\ValueObject\Address;
use XOne\Bundle\NotifierBundle\Exception\LogicException;
use XOne\Bundle\NotifierBundle\Mailer\PersistentMessageEmail;
use XOne\Bundle\NotifierBundle\Message\MessageOptions;
use XOne\Bundle\NotifierBundle\Message\PushMessage;
use XOne\Bundle\NotifierBundle\Model\MessageInterface;
use XOne\Bundle\NotifierBundle\Repository\MessageRepository;
use XOne\Bundle\NotifierBundle\Sender\NotifierMessageSender;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Factory\MessageFactory;
use Zenstruck\Foundry\Test\ResetDatabase;
use Zenstruck\Mailer\Test\InteractsWithMailer;
use Zenstruck\Mailer\Test\TestEmail;
use Zenstruck\Messenger\Test\InteractsWithMessenger;

/**
 * Note: the SMS and Chat channels are tested using their "fake" transports.
 * Therefore, messages sent to these channels are actually sent using the mailer.
 *
 * @see https://github.com/symfony/fake-sms-notifier/
 * @see https://github.com/symfony/fake-chat-notifier/
 */
class NotifierMessageSenderTest extends KernelTestCase
{
    use InteractsWithMessenger;
    use InteractsWithMailer;
    use ResetDatabase;

    public function testSendingMessageWithoutChannelsThrowsException()
    {
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Attempting to send a message without channel specified.');

        $this->createSender()->send($this->createMessage()->setChannel(null));
    }

    public function testSendingEmailMessageWithContent()
    {
        $message = $this->createMessage('Foo', 'Bar', 'email');
        $message->setTo([new Address('recipient@example.com')]);

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (PersistentMessageEmail $email) {
            $this->assertEquals('Bar', $email->getContext()['content']);
        });
    }

    public function testSendingEmailMessageWithDefaultFrom()
    {
        $message = $this->createMessage('Foo', 'Bar', 'email');
        $message->setTo([new Address('recipient@example.com')]);

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $email->assertFrom('sender@example.com');
        });
    }

    public function testSendingEmailMessageWithCustomFrom()
    {
        $message = $this->createMessage('Foo', 'Bar', 'email');
        $message->setFrom([new Address('custom-sender@example.com')]);
        $message->setTo([new Address('recipient@example.com')]);

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $email->assertFrom('custom-sender@example.com');
        });
    }

    public function testSendingEmailMessageWithoutRecipients()
    {
        $message = $this->createMessage('Foo', 'Bar', 'email');

        $this->expectException(NotifierLogicException::class);
        $this->expectExceptionMessage('The "email" channel is not supported.');

        $this->send($message);
    }

    public function testSendingEmailMessageWithMultipleRecipients()
    {
        $message = $this->createMessage('Foo', 'Bar', 'email');
        $message->addTo(new Address('recipient1@example.com'));
        $message->addTo(new Address('recipient2@example.com'));

        $this->send($message); // Note: expected queue count is 1 regardless of the number of recipients!

        $this->mailer()->assertEmailSentTo('recipient1@example.com', function (TestEmail $email) {
            $this->assertCount(2, $email->getTo());
            $this->assertEquals('recipient1@example.com', $email->getTo()[0]->getAddress());
            $this->assertEquals('recipient2@example.com', $email->getTo()[1]->getAddress());
        });

        $this->mailer()->assertEmailSentTo('recipient2@example.com', function (TestEmail $email) {
            $this->assertCount(2, $email->getTo());
            $this->assertEquals('recipient1@example.com', $email->getTo()[0]->getAddress());
            $this->assertEquals('recipient2@example.com', $email->getTo()[1]->getAddress());
        });
    }

    public function testSendingEmailMessageWithCc()
    {
        $message = $this->createMessage('Foo', 'Bar', 'email');
        $message->setTo([new Address('recipient@example.com')]);
        $message->setCc([new Address('foo@example.com', 'foo'), new Address('bar@example.com')]);

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $email
                ->assertCc('foo@example.com', 'foo')
                ->assertCc('bar@example.com')
            ;
        });
    }

    public function testSendingEmailMessageWithBcc()
    {
        $message = $this->createMessage('Foo', 'Bar', 'email');
        $message->setTo([new Address('recipient@example.com')]);
        $message->setBcc([new Address('foo@example.com', 'foo'), new Address('bar@example.com')]);

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $email
                ->assertBcc('foo@example.com', 'foo')
                ->assertBcc('bar@example.com')
            ;
        });
    }

    public function testSendingEmailMessageWithReplyTo()
    {
        $message = $this->createMessage('Foo', 'Bar', 'email');
        $message->setTo([new Address('recipient@example.com')]);
        $message->setReplyTo([new Address('foo@example.com', 'foo'), new Address('bar@example.com')]);

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $email
                ->assertReplyTo('foo@example.com', 'foo')
                ->assertReplyTo('bar@example.com')
            ;
        });
    }

    public function testSendingEmailMessageWithSender()
    {
        $message = $this->createMessage('Foo', 'Bar', 'email');
        $message->setTo([new Address('recipient@example.com')]);
        $message->setSender(new Address('foo@example.com', 'foo'));

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $this->assertEquals(new MimeAddress('foo@example.com', 'foo'), $email->getHeaders()->get('sender')->getBody());
        });
    }

    public function testSendingEmailMessageWithReturnPath()
    {
        $message = $this->createMessage('Foo', 'Bar', 'email');
        $message->setTo([new Address('recipient@example.com')]);
        $message->setReturnPath(new Address('foo@example.com', 'foo'));

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $this->assertEquals(new MimeAddress('foo@example.com', 'foo'), $email->getHeaders()->get('return-path')->getBody());
        });
    }

    public function testSendingEmailMessageWithoutPriority()
    {
        $message = $this->createMessage('Foo', 'Bar', 'email');
        $message->setTo([new Address('recipient@example.com')]);

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $this->assertFalse($email->getHeaders()->has('x-priority'));
        });
    }

    public function testSendingEmailMessageWithPriority()
    {
        $message = $this->createMessage('Foo', 'Bar', 'email');
        $message->setTo([new Address('recipient@example.com')]);
        $message->setPriority(5);

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $this->assertEquals('5 (Lowest)', $email->getHeaders()->get('x-priority')->getBody());
        });
    }

    public function testSendingEmailMessageWithAttachments()
    {
        $message = $this->createMessage(content: 'Foo Bar', channel: 'email');
        $message->setTo([new Address('recipient@example.com')]);

        $attachmentFactory = $this->getContainer()->get('public.x_one_notifier.factory.message_attachment');

        $message->addMessageAttachment($attachmentFactory->create(
            filename: __DIR__.'/../../Fixtures/Resources/attachment-image.png',
            contentType: 'image/png'
        ));

        $message->addMessageAttachment($attachmentFactory->create(
            filename: __DIR__.'/../../Fixtures/Resources/attachment-file.txt',
            name: 'foo',
        ));

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $mimeAttachments = $email->getAttachments();

            $this->assertCount(2, $mimeAttachments);
            $this->assertEquals('attachment-image.png', $mimeAttachments[0]->getFilename());
            $this->assertEquals('image/png', $mimeAttachments[0]->getContentType());
            $this->assertEquals('foo', $mimeAttachments[1]->getFilename());
        });
    }

    public function testSendingSmsMessageWithDefaultFrom()
    {
        $message = $this->createMessage(content: 'Foo Bar', channel: 'sms');
        $message->setTo([new Address('123456789')]);

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $email->assertFrom('sender@example.com');
        });
    }

    public function testSendingSmsMessageWithCustomFrom()
    {
        $message = $this->createMessage(content: 'Foo Bar', channel: 'sms');
        $message->setTo([new Address('123456789')]);
        $message->setFrom([new Address('custom-sender@example.com')]);

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $email->assertFrom('custom-sender@example.com');
        });
    }

    public function testSendingSmsMessageWithRecipient()
    {
        $message = $this->createMessage(content: 'Foo Bar', channel: 'sms');
        $message->setTo([new Address('123456789')]);
        $message->setFrom([new Address('custom-sender@example.com')]);

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $email->assertSubject('New SMS on phone number: 123456789');
        });
    }

    public function testSendingSmsMessageWithMultipleRecipients()
    {
        $message = $this->createMessage(content: 'Foo Bar', channel: 'sms');
        $message->setTo([new Address('123456789'), new Address('987654321')]);
        $message->setFrom([new Address('custom-sender@example.com')]);

        $this->send($message, expectedQueueCount: 2);
    }

    public function testSendingSmsMessageWithoutRecipients()
    {
        $message = $this->createMessage('Foo', 'Bar', 'sms');

        $this->expectException(NotifierLogicException::class);
        $this->expectExceptionMessage('The "sms" channel needs a Recipient.');

        $this->send($message);
    }

    public function testSendingChatMessageWithRecipientIdSetDirectly()
    {
        $message = $this->createMessage(content: 'Foo Bar', channel: 'chat');
        $message->setRecipientId('test-recipient-id');

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $email->assertSubject('New Chat message for recipient: test-recipient-id');
        });
    }

    public function testSendingChatMessageWithRecipientIdInOptions()
    {
        $message = $this->createMessage(content: 'Foo Bar', channel: 'chat');
        $message->setOptions(new MessageOptions(recipientId: 'test-recipient-id'));

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $email->assertSubject('New Chat message for recipient: test-recipient-id');
        });
    }

    public function testSendingChatMessageWithoutRecipientId()
    {
        $message = $this->createMessage(content: 'Foo Bar', channel: 'chat');

        $this->send($message);

        $this->mailer()->assertEmailSentTo('recipient@example.com', function (TestEmail $email) {
            $email->assertSubject('New Chat message without specified recipient!');
        });
    }

    public function testSendingPushMessage()
    {
        $message = $this->createMessage(content: 'Foo Bar', channel: 'push');
        $message->setOptions(new MessageOptions(recipientId: 'test-recipient-id'));

        $this->createSender()->send($message);

        $this->transport()->queue()->assertCount(1);

        /** @var PushMessage $pushMessage */
        $pushMessage = current($this->transport()->queue()->messages(PushMessage::class));

        $this->assertEquals('test-recipient-id', $pushMessage->getRecipientId());
        $this->assertEquals('Foo Bar', $pushMessage->getContent());

        $this->transport()->process();
        $this->transport()->queue()->assertEmpty();
    }

    public function testSendingMessageWithInvalidChannel()
    {
        $this->expectException(NotifierLogicException::class);
        $this->expectExceptionMessage('The "invalid-channel" channel does not exist.');

        $this->createSender()->send($this->createMessage(channel: 'invalid-channel'));
    }

    private function createSender(): NotifierMessageSender
    {
        $notifier = $this->getContainer()->get('notifier');
        $messageRepository = $this->getContainer()->get(MessageRepository::class);

        return new NotifierMessageSender($notifier, $messageRepository);
    }

    private function createMessage(string $subject = null, string $content = null, string $channel = null): MessageInterface
    {
        $messageFactory = $this->getContainer()->get('public.x_one_notifier.factory.message');

        return $messageFactory->create(
            subject: $subject,
            content: $content,
            channel: $channel,
        );
    }

    private function send(MessageInterface $message, int $expectedQueueCount = 1): void
    {
        $this->createSender()->send($message);

        MessageFactory::assert()->count(1);

        $this->transport()->queue()->assertCount($expectedQueueCount);
        $this->transport()->process();
        $this->transport()->queue()->assertEmpty();

        $this->mailer()->assertSentEmailCount($expectedQueueCount);
    }
}

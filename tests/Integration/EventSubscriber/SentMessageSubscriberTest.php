<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Integration\EventSubscriber;

use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\Event\SentMessageEvent as MailerSentMessageEvent;
use Symfony\Component\Mailer\SentMessage as MailerSentMessage;
use Symfony\Component\Mime\Address as MimeAddress;
use Symfony\Component\Notifier\Event\SentMessageEvent as NotifierSentMessageEvent;
use Symfony\Component\Notifier\Message\SentMessage as NotifierSentMessage;
use XOne\Bundle\NotifierBundle\Entity\ValueObject\Address;
use XOne\Bundle\NotifierBundle\Mailer\PersistentMessageEnvelope;
use XOne\Bundle\NotifierBundle\Message\EmailMessage;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\Message;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Factory\MessageFactory;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Factory\MessageTransactionFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class SentMessageSubscriberTest extends EventSubscriberTestCase
{
    use ResetDatabase;
    use Factories;

    public function testCreatingMessageTransactionOnMailerSentMessageEventWithNonPersistentMessageAwareEnvelope()
    {
        $envelope = $this->createMock(Envelope::class);

        $sentMessage = $this->createMock(MailerSentMessage::class);
        $sentMessage->method('getEnvelope')->willReturn($envelope);

        $this->dispatch(new MailerSentMessageEvent($sentMessage));

        MessageTransactionFactory::assert()->count(0);
    }

    public function testCreatingMessageTransactionOnMailerSentMessageEventWithNonExistentPersistentMessageId()
    {
        $envelope = $this->createMock(PersistentMessageEnvelope::class);
        $envelope->method('getPersistentMessageId')->willReturn(0);

        $sentMessage = $this->createMock(MailerSentMessage::class);
        $sentMessage->method('getEnvelope')->willReturn($envelope);

        $this->dispatch(new MailerSentMessageEvent($sentMessage));

        MessageTransactionFactory::assert()->count(0);
    }

    public function testCreatingMessageTransactionOnMailerSentMessageEvent()
    {
        $message = MessageFactory::createOne();

        $envelope = $this->createMock(PersistentMessageEnvelope::class);
        $envelope->method('getPersistentMessageId')->willReturn(1);
        $envelope->method('getSender')->willReturn(new MimeAddress('foo@example.com'));

        $sentMessage = $this->createMock(MailerSentMessage::class);
        $sentMessage->method('getEnvelope')->willReturn($envelope);
        $sentMessage->method('getMessageId')->willReturn('foo bar');
        $sentMessage->method('getDebug')->willReturn('lorem ipsum dolor sit amet');

        $this->dispatch(new MailerSentMessageEvent($sentMessage));

        MessageTransactionFactory::assert()
            ->count(1)
            ->exists([
                'message' => $message,
                'successful' => true,
                'transportMessageId' => 'foo bar',
                'exception' => null,
                'debug' => 'lorem ipsum dolor sit amet',
            ]);
    }

    public function testCreatingMessageTransactionOnNotifierSentMessageEvent()
    {
        $message = MessageFactory::createOne();

        $emailMessage = $this->createMock(EmailMessage::class);
        $emailMessage->method('getPersistentMessageId')->willReturn(1);

        $sentMessage = $this->createMock(NotifierSentMessage::class);
        $sentMessage->method('getOriginalMessage')->willReturn($emailMessage);
        $sentMessage->method('getMessageId')->willReturn('foo bar');

        $this->dispatch(new NotifierSentMessageEvent($sentMessage));

        MessageTransactionFactory::assert()
            ->count(1)
            ->exists([
                'message' => $message,
                'successful' => true,
                'transportMessageId' => 'foo bar',
                'exception' => null,
                'debug' => null, // Only mailer-related events contains debug!
            ]);
    }

    public function testUpdatingFromAddressOnMailerSentMessageEvent()
    {
        $message = MessageFactory::createOne();

        $envelope = $this->createMock(PersistentMessageEnvelope::class);
        $envelope->method('getPersistentMessageId')->willReturn(1);
        $envelope->method('getSender')->willReturn(new MimeAddress('foo@example.com'));

        $sentMessage = $this->createMock(MailerSentMessage::class);
        $sentMessage->method('getEnvelope')->willReturn($envelope);

        $this->dispatch(new MailerSentMessageEvent($sentMessage));

        MessageFactory::assert()->count(1);

        $this->assertEquals(MessageFactory::find($message->getId())->getFrom(), [new Address('foo@example.com')]);
    }

    public function testUpdatingFromAddressAbortedWhenMessageAlreadyHasFrom()
    {
        /** @var Message|Proxy $message */
        $message = MessageFactory::new()->withoutPersisting()->create();
        $message->addFrom(new Address('bar@example.com'));
        $message->save();

        $envelope = $this->createMock(PersistentMessageEnvelope::class);
        $envelope->method('getPersistentMessageId')->willReturn(1);
        $envelope->method('getSender')->willReturn(new MimeAddress('foo@example.com'));

        $sentMessage = $this->createMock(MailerSentMessage::class);
        $sentMessage->method('getEnvelope')->willReturn($envelope);

        $this->dispatch(new MailerSentMessageEvent($sentMessage));

        MessageFactory::assert()->count(1);

        $this->assertEquals(MessageFactory::find($message->getId())->getFrom(), [new Address('bar@example.com')]);
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Integration\EventSubscriber;

use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Mailer\Event\FailedMessageEvent as MailerFailedMessageEvent;
use Symfony\Component\Messenger\Envelope as MessengerEnvelope;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Mime\RawMessage;
use Symfony\Component\Notifier\Event\FailedMessageEvent as NotifierFailedMessageEvent;
use XOne\Bundle\NotifierBundle\Mailer\PersistentMessageEmail;
use XOne\Bundle\NotifierBundle\Message\EmailMessage;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Factory\MessageFactory;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Factory\MessageTransactionFactory;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class FailedMessageSubscriberTest extends EventSubscriberTestCase
{
    use ResetDatabase;
    use Factories;

    public function testCreatingMessageTransactionOnMailerFailedMessageEventWithNonPersistentMessageAwareMessage()
    {
        $email = $this->createMock(RawMessage::class);

        $this->dispatch(new MailerFailedMessageEvent($email, new \Exception()));

        MessageTransactionFactory::assert()->count(0);
    }

    public function testCreatingMessageTransactionOnMailerFailedMessageEventWithNonExistentPersistentMessageId()
    {
        $email = $this->createMock(PersistentMessageEmail::class);
        $email->method('getPersistentMessageId')->willReturn(0);

        $this->dispatch(new MailerFailedMessageEvent($email, new \Exception()));

        MessageTransactionFactory::assert()->count(0);
    }

    public function testCreatingMessageTransactionOnMailerFailedMessageEvent()
    {
        $message = MessageFactory::createOne();

        $email = $this->createMock(PersistentMessageEmail::class);
        $email->method('getPersistentMessageId')->willReturn(1);

        $exception = new \Exception('Foo bar');

        $this->dispatch(new MailerFailedMessageEvent($email, $exception));

        MessageTransactionFactory::assert()
            ->count(1)
            ->exists([
                'message' => $message,
                'successful' => false,
                'transportMessageId' => null,
                'exception' => 'Foo bar',
                'debug' => null,
            ]);
    }

    public function testCreatingMessageTransactionOnNotifierFailedMessageEvent()
    {
        $message = MessageFactory::createOne();

        $email = $this->createMock(EmailMessage::class);
        $email->method('getPersistentMessageId')->willReturn(1);

        $exception = new \Exception('Foo bar');

        $this->dispatch(new NotifierFailedMessageEvent($email, $exception));

        MessageTransactionFactory::assert()
            ->count(1)
            ->exists([
                'message' => $message,
                'successful' => false,
                'transportMessageId' => null,
                'exception' => 'Foo bar',
                'debug' => null,
            ]);
    }

    public function testCreatingMessageTransactionOnMessengerHandlerFailedExceptionEvent()
    {
        $message = MessageFactory::createOne();

        $email = $this->createMock(EmailMessage::class);
        $email->method('getPersistentMessageId')->willReturn(1);

        /** @noinspection PhpUnitInvalidMockingEntityInspection due to BypassFinalHook */
        $envelope = $this->createMock(MessengerEnvelope::class);
        $envelope->method('getMessage')->willReturn($email);

        $exception = new HandlerFailedException($envelope, [new \Exception('Foo bar')]);

        $event = new ExceptionEvent(
            $this->createMock(KernelInterface::class),
            $this->getMockRequest(),
            KernelInterface::MAIN_REQUEST,
            $exception
        );

        $this->dispatch($event, 'kernel.exception');

        MessageTransactionFactory::assert()
            ->count(1)
            ->exists([
                'message' => $message,
                'successful' => false,
                'transportMessageId' => null,
                'exception' => sprintf('Handling "%s" failed: Foo bar', get_class($email)),
                'debug' => null,
            ]);
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Integration\EventSubscriber;

use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

abstract class EventSubscriberTestCase extends KernelTestCase
{
    protected function dispatch(object $event, string $eventName = null): void
    {
        $this->getEventDispatcher()->dispatch($event, $eventName);
    }

    protected function getEventDispatcher(): EventDispatcherInterface
    {
        return $this->getContainer()->get('public.event_dispatcher');
    }

    protected function getMockRequest(): MockObject|Request
    {
        $request = $this->createMock(Request::class);
        $request->attributes = new ParameterBag();

        return $request;
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Integration\Factory;

use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\Message;

class MessageFactoryTest extends AbstractFactoryTestCase
{
    protected function getFactoryServiceName(): string
    {
        return 'public.x_one_notifier.factory.message';
    }

    protected function getEntityClass(): string
    {
        return Message::class;
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Integration\Factory;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class AbstractFactoryTestCase extends KernelTestCase
{
    abstract protected function getFactoryServiceName(): string;

    abstract protected function getEntityClass(): string;

    public function testFactoryUsesEntityClassFromBundleConfiguration()
    {
        $factory = $this->getContainer()->get($this->getFactoryServiceName());

        $property = new \ReflectionProperty($factory, 'class');

        $this->assertEquals($this->getEntityClass(), $property->getValue($factory));
    }
}

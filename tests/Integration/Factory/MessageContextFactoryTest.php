<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Integration\Factory;

use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\MessageContext;

class MessageContextFactoryTest extends AbstractFactoryTestCase
{
    protected function getFactoryServiceName(): string
    {
        return 'public.x_one_notifier.factory.message_context';
    }

    protected function getEntityClass(): string
    {
        return MessageContext::class;
    }
}

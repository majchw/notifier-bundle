<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Integration\Factory;

use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\MessageAttachment;

class MessageAttachmentFactoryTest extends AbstractFactoryTestCase
{
    protected function getFactoryServiceName(): string
    {
        return 'public.x_one_notifier.factory.message_attachment';
    }

    protected function getEntityClass(): string
    {
        return MessageAttachment::class;
    }
}

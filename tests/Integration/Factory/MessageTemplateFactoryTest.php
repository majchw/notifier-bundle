<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Integration\Factory;

use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\MessageTemplate;

class MessageTemplateFactoryTest extends AbstractFactoryTestCase
{
    protected function getFactoryServiceName(): string
    {
        return 'public.x_one_notifier.factory.message_template';
    }

    protected function getEntityClass(): string
    {
        return MessageTemplate::class;
    }
}

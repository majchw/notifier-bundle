<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Fixtures\Message;

use Symfony\Component\Notifier\Message\MessageOptionsInterface;

class CustomMessageOptions implements MessageOptionsInterface
{
    public function __construct(private readonly array $options = [])
    {
    }

    public function toArray(): array
    {
        return $this->options;
    }

    public function getRecipientId(): ?string
    {
        return $this->options['recipient-id'];
    }
}

<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Fixtures\Templating\Variables;

use XOne\Bundle\NotifierBundle\Templating\Variables\VariableValueProviderInterface;

class ExceptionThrowingVariableValueProvider implements VariableValueProviderInterface
{
    public function __construct(private readonly ?\Throwable $exception = null)
    {
    }

    public function getValue(string $name, mixed $subject = null): mixed
    {
        throw $this->exception;
    }
}

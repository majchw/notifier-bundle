<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Fixtures\Templating\Variables;

use XOne\Bundle\NotifierBundle\Templating\Variables\VariableValueProviderInterface;

class SuccessfulVariableValueProvider implements VariableValueProviderInterface
{
    public function __construct(private readonly mixed $value = null)
    {
    }

    public function getValue(string $name, mixed $subject = null): mixed
    {
        return $this->value;
    }
}

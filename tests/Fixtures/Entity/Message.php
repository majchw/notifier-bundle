<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity;

use Doctrine\ORM\Mapping as ORM;
use XOne\Bundle\NotifierBundle\Entity\Message as BaseMessage;

#[ORM\Entity]
#[ORM\Table(name: 'notifier_message')]
class Message extends BaseMessage
{
}

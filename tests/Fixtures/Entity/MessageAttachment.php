<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity;

use Doctrine\ORM\Mapping as ORM;
use XOne\Bundle\NotifierBundle\Entity\MessageAttachment as BaseMessageAttachment;

#[ORM\Entity]
#[ORM\Table(name: 'notifier_message_attachment')]
class MessageAttachment extends BaseMessageAttachment
{
}

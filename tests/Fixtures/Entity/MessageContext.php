<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity;

use Doctrine\ORM\Mapping as ORM;
use XOne\Bundle\NotifierBundle\Entity\MessageContext as BaseMessageContext;

#[ORM\Entity]
#[ORM\Table(name: 'notifier_message_context')]
class MessageContext extends BaseMessageContext
{
}

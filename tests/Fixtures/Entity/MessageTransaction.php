<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity;

use Doctrine\ORM\Mapping as ORM;
use XOne\Bundle\NotifierBundle\Entity\MessageTransaction as BaseMessageTransaction;

#[ORM\Entity]
#[ORM\Table(name: 'notifier_message_transaction')]
class MessageTransaction extends BaseMessageTransaction
{
}

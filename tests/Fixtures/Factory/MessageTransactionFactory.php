<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Fixtures\Factory;

use Doctrine\ORM\EntityRepository;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\MessageTransaction;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<MessageTransaction>
 *
 * @method static MessageTransaction|Proxy         createOne(array $attributes = [])
 * @method static MessageTransaction[]|Proxy[]     createMany(int $number, array|callable $attributes = [])
 * @method static MessageTransaction|Proxy         find(object|array|mixed $criteria)
 * @method static MessageTransaction|Proxy         findOrCreate(array $attributes)
 * @method static MessageTransaction|Proxy         first(string $sortedField = 'id')
 * @method static MessageTransaction|Proxy         last(string $sortedField = 'id')
 * @method static MessageTransaction|Proxy         random(array $attributes = [])
 * @method static MessageTransaction|Proxy         randomOrCreate(array $attributes = []))
 * @method static MessageTransaction[]|Proxy[]     all()
 * @method static MessageTransaction[]|Proxy[]     findBy(array $attributes)
 * @method static MessageTransaction[]|Proxy[]     randomSet(int $number, array $attributes = []))
 * @method static MessageTransaction[]|Proxy[]     randomRange(int $min, int $max, array $attributes = []))
 * @method static EntityRepository|RepositoryProxy repository()
 * @method        MessageTransaction|Proxy         create(array|callable $attributes = [])
 */
class MessageTransactionFactory extends ModelFactory
{
    protected static function getClass(): string
    {
        return MessageTransaction::class;
    }

    protected function getDefaults(): array
    {
        return [];
    }
}

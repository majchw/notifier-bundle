<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Fixtures\Factory;

use Doctrine\ORM\EntityRepository;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\Message;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Message>
 *
 * @method static Message|Proxy                    createOne(array $attributes = [])
 * @method static Message[]|Proxy[]                createMany(int $number, array|callable $attributes = [])
 * @method static Message|Proxy                    find(object|array|mixed $criteria)
 * @method static Message|Proxy                    findOrCreate(array $attributes)
 * @method static Message|Proxy                    first(string $sortedField = 'id')
 * @method static Message|Proxy                    last(string $sortedField = 'id')
 * @method static Message|Proxy                    random(array $attributes = [])
 * @method static Message|Proxy                    randomOrCreate(array $attributes = []))
 * @method static Message[]|Proxy[]                all()
 * @method static Message[]|Proxy[]                findBy(array $attributes)
 * @method static Message[]|Proxy[]                randomSet(int $number, array $attributes = []))
 * @method static Message[]|Proxy[]                randomRange(int $min, int $max, array $attributes = []))
 * @method static EntityRepository|RepositoryProxy repository()
 * @method        Message|Proxy                    create(array|callable $attributes = [])
 */
class MessageFactory extends ModelFactory
{
    protected static function getClass(): string
    {
        return Message::class;
    }

    protected function getDefaults(): array
    {
        return [
            'channel' => 'email',
        ];
    }
}

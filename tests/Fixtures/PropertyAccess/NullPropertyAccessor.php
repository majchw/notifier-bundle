<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Fixtures\PropertyAccess;

use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyAccess\PropertyPathInterface;

class NullPropertyAccessor implements PropertyAccessorInterface
{
    public function setValue(object|array &$objectOrArray, PropertyPathInterface|string $propertyPath, mixed $value): void
    {
        throw new \Exception('Not supported');
    }

    public function getValue(object|array $objectOrArray, PropertyPathInterface|string $propertyPath): mixed
    {
        return null;
    }

    public function isWritable(object|array $objectOrArray, PropertyPathInterface|string $propertyPath): bool
    {
        throw new \Exception('Not supported');
    }

    public function isReadable(object|array $objectOrArray, PropertyPathInterface|string $propertyPath): bool
    {
        return true;
    }
}

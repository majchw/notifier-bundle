<?php

declare(strict_types=1);

namespace XOne\Bundle\NotifierBundle\Tests\Fixtures;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Psr\Log\NullLogger;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Mailer\Messenger\SendEmailMessage;
use XOne\Bundle\NotifierBundle\Message\ChatMessage;
use XOne\Bundle\NotifierBundle\Message\PushMessage;
use XOne\Bundle\NotifierBundle\Message\SmsMessage;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\Message;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\MessageAttachment;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\MessageContext;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\MessageTemplate;
use XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity\MessageTransaction;
use XOne\Bundle\NotifierBundle\XOneNotifierBundle;
use Zenstruck\Mailer\Test\ZenstruckMailerTestBundle;
use Zenstruck\Messenger\Test\ZenstruckMessengerTestBundle;

final class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function registerBundles(): iterable
    {
        yield new FrameworkBundle();
        yield new DoctrineBundle();
        yield new TwigBundle();
        yield new ZenstruckMessengerTestBundle();
        yield new ZenstruckMailerTestBundle();
        yield new XOneNotifierBundle();
    }

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->extension('framework', [
            'test' => true,
            'secret' => 'S3CRET',
            'notifier' => [
                'texter_transports' => [
                    'fakesms' => 'fakesms+email://default?to=recipient@example.com&from=sender@example.com',
                ],
                'chatter_transports' => [
                    'fakechat' => 'fakechat+email://default?to=recipient@example.com&from=sender@example.com',
                ],
            ],
            'messenger' => [
                'transports' => [
                    'async' => 'test://',
                ],
                'routing' => [
                    SendEmailMessage::class => 'async',
                    SmsMessage::class => 'async',
                    PushMessage::class => 'async',
                    ChatMessage::class => 'async',
                ],
            ],
            'mailer' => [
                'envelope' => [
                    'sender' => 'sender@example.com',
                ],
            ],
        ]);

        $container->extension('doctrine', [
            'dbal' => ['url' => '%env(resolve:DATABASE_URL)%'],
            'orm' => [
                'auto_generate_proxy_classes' => true,
                'auto_mapping' => true,
                'mappings' => [
                    'App' => [
                        'is_bundle' => false,
                        'dir' => '%kernel.project_dir%/tests/Fixtures/Entity',
                        'prefix' => 'XOne\Bundle\NotifierBundle\Tests\Fixtures\Entity',
                        'type' => 'attribute',
                        'alias' => 'App',
                    ],
                ],
                'resolve_target_entities' => [
                    'XOne\Bundle\NotifierBundle\Entity\MessageInterface' => 'XOne\Bundle\NotifierBundle\Entity\Message',
                    'XOne\Bundle\NotifierBundle\Entity\MessageTransactionInterface' => 'XOne\Bundle\NotifierBundle\Entity\MessageTransaction',
                    'XOne\Bundle\NotifierBundle\Entity\MessageAttachmentInterface' => 'XOne\Bundle\NotifierBundle\Entity\MessageAttachment',
                    'XOne\Bundle\NotifierBundle\Entity\MessageContextInterface' => 'XOne\Bundle\NotifierBundle\Entity\MessageContext',
                    'XOne\Bundle\NotifierBundle\Entity\MessageTemplateInterface' => 'XOne\Bundle\NotifierBundle\Entity\MessageTemplate',
                ],
            ],
        ]);

        $container->extension('x_one_notifier', [
            'entities' => [
                'message' => Message::class,
                'message_attachment' => MessageAttachment::class,
                'message_transaction' => MessageTransaction::class,
                'message_context' => MessageContext::class,
                'message_template' => MessageTemplate::class,
            ],
        ]);

        $services = $container->services();

        $services->defaults()->autowire()->autoconfigure();
        $services->alias('public.notifier', 'notifier')->public();
        $services->alias('public.event_dispatcher', 'event_dispatcher')->public();
        $services->alias('public.x_one_notifier.factory.message', 'x_one_notifier.factory.message')->public();
        $services->alias('public.x_one_notifier.factory.message_transaction', 'x_one_notifier.factory.message_transaction')->public();
        $services->alias('public.x_one_notifier.factory.message_template', 'x_one_notifier.factory.message_template')->public();
        $services->alias('public.x_one_notifier.factory.message_context', 'x_one_notifier.factory.message_context')->public();
        $services->alias('public.x_one_notifier.factory.message_attachment', 'x_one_notifier.factory.message_attachment')->public();

        $services
            // disable logging errors to the console
            ->set('logger', NullLogger::class)
            ->load(__NAMESPACE__.'\\', __DIR__)
            ->exclude(['Kernel.php'])
        ;
    }
}
